namespace TaxiShareEntity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Fares",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        From = c.String(nullable: false),
                        To = c.String(nullable: false),
                        When = c.DateTime(nullable: false),
                        Cost = c.Double(nullable: false),
                        Taxi_ID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Taxis", t => t.Taxi_ID, cascadeDelete: true)
                .Index(t => t.Taxi_ID);
            
            CreateTable(
                "dbo.Points",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        CoordX = c.Double(nullable: false),
                        CoordY = c.Double(nullable: false),
                        Votes = c.Int(nullable: false),
                        IsDefault = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Taxis",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        DriverName = c.String(nullable: false),
                        DriverSurname = c.String(nullable: false),
                        PhoneNumber = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Surname = c.String(nullable: false),
                        Login = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        Gender = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PointFares",
                c => new
                    {
                        Point_ID = c.Long(nullable: false),
                        Fare_ID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.Point_ID, t.Fare_ID })
                .ForeignKey("dbo.Points", t => t.Point_ID, cascadeDelete: true)
                .ForeignKey("dbo.Fares", t => t.Fare_ID, cascadeDelete: true)
                .Index(t => t.Point_ID)
                .Index(t => t.Fare_ID);
            
            CreateTable(
                "dbo.UserFares",
                c => new
                    {
                        User_ID = c.Long(nullable: false),
                        Fare_ID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_ID, t.Fare_ID })
                .ForeignKey("dbo.Users", t => t.User_ID, cascadeDelete: true)
                .ForeignKey("dbo.Fares", t => t.Fare_ID, cascadeDelete: true)
                .Index(t => t.User_ID)
                .Index(t => t.Fare_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserFares", "Fare_ID", "dbo.Fares");
            DropForeignKey("dbo.UserFares", "User_ID", "dbo.Users");
            DropForeignKey("dbo.Fares", "Taxi_ID", "dbo.Taxis");
            DropForeignKey("dbo.PointFares", "Fare_ID", "dbo.Fares");
            DropForeignKey("dbo.PointFares", "Point_ID", "dbo.Points");
            DropIndex("dbo.UserFares", new[] { "Fare_ID" });
            DropIndex("dbo.UserFares", new[] { "User_ID" });
            DropIndex("dbo.PointFares", new[] { "Fare_ID" });
            DropIndex("dbo.PointFares", new[] { "Point_ID" });
            DropIndex("dbo.Fares", new[] { "Taxi_ID" });
            DropTable("dbo.UserFares");
            DropTable("dbo.PointFares");
            DropTable("dbo.Users");
            DropTable("dbo.Taxis");
            DropTable("dbo.Points");
            DropTable("dbo.Fares");
        }
    }
}

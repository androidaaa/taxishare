﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TaxiShareEntity.Domain
{
    public class Taxi : Entity
    {
        [Required]
        public virtual string DriverName { get; set; }
        [Required]
        public virtual string DriverSurname { get; set; }
        [Required]
        public virtual string PhoneNumber { get; set; }
        [Required]
        public virtual ICollection<Fare> FareList { get; set; }
    }
}

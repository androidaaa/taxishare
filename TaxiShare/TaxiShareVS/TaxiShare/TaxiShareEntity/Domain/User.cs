﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TaxiShareEntity.Domain
{
    public class User : Entity
    {
        [Required]
        public virtual string Name { get; set; }
        [Required]
        public virtual string Surname { get; set; }
        [Required]
        public virtual string Login { get; set; }
        [Required]
        public virtual string Password { get; set; }
        [Required]
        public virtual bool Gender { get; set; }
        
        public virtual ICollection<Fare> Courses { get; set; }
    }
}

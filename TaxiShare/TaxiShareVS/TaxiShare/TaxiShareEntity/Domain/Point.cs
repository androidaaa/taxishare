﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TaxiShareEntity.Domain
{
    public class Point : Entity
    {
        [Required]
        public virtual string Name { get; set; }

        [Required]
        public virtual double CoordX { get; set; }

        [Required]
        public virtual double CoordY { get; set; }
        public virtual int Votes { get; set; }
        [Required]
        public virtual bool IsDefault { get; set; }

     
        public virtual ICollection<Fare> FareList { get; set; }

    }
}

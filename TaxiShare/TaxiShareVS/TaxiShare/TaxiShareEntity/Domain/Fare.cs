﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TaxiShareEntity.Domain
{
    public class Fare:Entity
    {
        [Required]

        public virtual string From { get; set; }
        [Required]
        public virtual string To { get; set; }
        [Required]
        public virtual DateTime When { get; set; }

        public virtual double Cost { get; set; }
        [Required]
        public virtual ICollection<User> UserList { get; set; }
        [Required]
        public virtual ICollection<Point> Checkpoints { get; set; }
        [Required]
        public virtual Taxi Taxi { get; set; }
    }
}

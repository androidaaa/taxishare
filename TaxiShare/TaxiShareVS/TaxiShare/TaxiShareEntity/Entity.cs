﻿using System.ComponentModel.DataAnnotations;

namespace TaxiShareEntity
{
    public abstract class Entity
    {
        [Key]
        public long ID { get; set; }
    }
}

﻿using System;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using TaxiShareEntity.Domain;

namespace TaxiShareEntity
{
    public class EntityContext : DbContext
    {
        static EntityContext()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<EntityContext>());
        }
        public EntityContext() : base("TaxiShareDataAzure")//TaxiShareData
        {
            Database.SetInitializer<EntityContext>(new DropCreateDatabaseIfModelChanges<EntityContext>());


        }
        public DbSet<Taxi> Taxis { get; set; }
        public DbSet<Fare> Fares { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Point> Points { get; set; }

        //    protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //    {
        //        modelBuilder.Entity<Fare>()
        //.HasRequired(t => t.From)
        //.WithMany(t=>t.FareList)
        //.WillCascadeOnDelete(false);

        //    }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Web.Http;
using TaxiShareServer.Models;
using WebApi2.Models;
using WebApi2.Services;

namespace WebApi2.Controllers
{
    public class PointController : ApiController
    {
        private IPointService _service;
        public PointController()
        {
            _service = new PointService();
        }

        public PointController(IPointService service)
        {
            _service = service;
        }

        // GET: api/Point
        [HttpGet]
        public IHttpActionResult Get()
        {
            IEnumerable<PointDetailsDTO> points = _service.GetPoints();
            if (points == null)
                return NotFound();
            return Ok(points);
        }

        // GET: api/Point/5
        public IHttpActionResult Get(long id)
        {
            PointDetailsDTO point = _service.GetPoint(id);
            if (point == null)
                return NotFound();
            return Ok(point);
        }

        // POST: api/Point
        [HttpPost]
        public IHttpActionResult Post([FromBody]PointDetailsDTO point)
        {
            if (point == null)
            {
                return Content(HttpStatusCode.BadRequest, "Bad request");
            }

            var context = new ValidationContext(point);
            List<ValidationResult> list = new List<ValidationResult>();
            if (!Validator.TryValidateObject(point, context, list, true))
            {
                if (list.Any())
                {
                    return Content(HttpStatusCode.BadRequest, list.First().ErrorMessage);
                }
            }

            PointDetailsDTO newPoint = _service.Add(point);
            if (newPoint == null)
                return NotFound();
            return Ok(newPoint);
        }

        [HttpPost]
        [Route("api/Point/Search")]
        public IHttpActionResult Search([FromBody] SearchPointDTO searchPoint)
        {
            if (searchPoint == null)
            {
                return Content(HttpStatusCode.BadRequest, "Bad request");
            }

            var newPoints = _service.Search(searchPoint);
            if (newPoints == null || !newPoints.Any())
                return NotFound();
            return Ok(newPoints);
        }
    }
}

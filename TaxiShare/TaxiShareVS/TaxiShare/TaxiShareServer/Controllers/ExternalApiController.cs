﻿using System.Collections.Generic;
using System.Web.Http;

namespace TaxiShareServer.Controllers
{
    public class ExternalApiController : ApiController
    {
        // GET: api/ExternalApi
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/ExternalApi/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/ExternalApi
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/ExternalApi/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ExternalApi/5
        public void Delete(int id)
        {
        }
    }
}

﻿using System.Collections.Generic;
using TaxiShareServer.Services;
using TaxiShareServer.Models;
using WebApi2.Controllers;
using System.Web.Http;

namespace TaxiShareServer.Controllers
{
    [RoutePrefix("api/Fare")]
    public class FareController : TaxiController
    {
        private IFareService service;
        public FareController(IFareService _service)
        {
            service = _service;
        }
        public FareController()
        {
            service = new FareService();
        }
       [HttpGet]
       [Route("GetOne/{id}")]
        public IHttpActionResult GetFare(long id)
        {
            FareDetailsDTO result = service.GetDetails(id);
            return result == null ? (IHttpActionResult)this.NotFound() : this.Ok(result);
        }

        [Route("AllFares")]
        public IHttpActionResult GetFares()
        {
            List<FareDTO> result = service.GetFares();
            return result == null ? (IHttpActionResult)this.NotFound() : this.Ok(result);
        }
     
        [HttpPost]
        [Route("PostFare")]
        public void PostFare([FromBody] FareNewDTO dto)
        {

            service.AddFare(dto);
        }
        [HttpPost]
        [Route("Join")]
        public void JoinFare([FromBody] FareNewDTO dto)
        {
            service.JoinFare(dto);
        }
        [HttpPost]
        [Route("Search")]
        public IHttpActionResult SearchFare([FromBody] FareDTO dto)
        {
            List<FareDTO> result = service.SearchFare(dto);
            return result == null ? (IHttpActionResult)this.NotFound() : this.Ok(result);
        }
    }
}

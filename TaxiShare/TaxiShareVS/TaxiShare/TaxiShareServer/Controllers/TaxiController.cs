﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Web.Http;
using TaxiShareApp.Services;
using TaxiShareServer.Models;
using WebApi2.Models;

namespace WebApi2.Controllers
{
    public class TaxiController : ApiController
    {
        private readonly ITaxiService _service;

        public TaxiController()
        {
            _service = new TaxiService();
        }

        public TaxiController(ITaxiService service)
        {
            _service = service;
        }

        // GET api/values
        [HttpGet]
        public IHttpActionResult Get()
        {
            IEnumerable<TaxiDetailsDTO> taxis = _service.GetTaxis();
            if (taxis == null)
                return NotFound();
            return Ok(taxis);
        }

        // GET api/values/5
        public IHttpActionResult Get(long id)
        {
            TaxiDetailsDTO taxi = _service.GetTaxi(id);
            if (taxi == null)
                return NotFound();
            return Ok(taxi);
        }

        [HttpPost]
        // POST api/values
        public IHttpActionResult Post([FromBody]NewTaxiDTO taxi)
        {
            if (taxi == null)
            {
                return Content(HttpStatusCode.BadRequest, "Bad request");
            }

            var context = new ValidationContext(taxi);
            List<ValidationResult> list = new List<ValidationResult>();
            if (!Validator.TryValidateObject(taxi, context, list, true))
            {
                if (list.Any())
                {
                    return Content(HttpStatusCode.BadRequest, list.First().ErrorMessage);
                }
            }

            TaxiDetailsDTO newTaxi = _service.Add(taxi);
            if (newTaxi == null)
                return NotFound();
            return Ok(newTaxi);
        }

        [HttpPost]
        [Route("api/Taxi/Search")]
        public IHttpActionResult Search([FromBody] SearchTaxiDTO searchTaxi)
        {
            if (searchTaxi == null)
            {
                return Content(HttpStatusCode.BadRequest, "Bad request");
            }

            var newTaxis = _service.Search(searchTaxi);
            if (newTaxis == null || !newTaxis.Any())
                return NotFound();
            return Ok(newTaxis);
        }

    }
}

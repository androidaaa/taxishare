﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;
using TaxiShareServer.Helpers;
using TaxiShareServer.Models;
using TaxiShareServer.Services;
using WebApi2.Controllers;


namespace TaxiShareServer.Controllers
{
    [RoutePrefix("api/User")]
    public class UserController : TaxiController
    {
        private IUserService service;
        public UserController(IUserService _service)
        {
            service = _service;
        }
        public UserController()
        {
            service = new UserService();
        }
        [Route("AllUsers")]
        public IHttpActionResult Get()
        {
            List<UserDTO> result = service.GetUsers();
            return result == null ? (IHttpActionResult)this.NotFound() : this.Ok(result);
        }
       // [Route("api/user/getid/{id}")]
       [Route("GetOne/{id}")]
        public IHttpActionResult Get(long id)
        {
            UserDetailsDTO result = service.GetDetails(id);
            return result == null ? (IHttpActionResult)this.NotFound() : this.Ok(result);
        }
        [Route("GetId")]
        public IHttpActionResult Get(String login)
        {
            long result = service.GetId(login);
            return result == 0 ? (IHttpActionResult) this.NotFound() : this.Ok(result);
        }

        [HttpPost]
        [Route("PostUser")]
        public IHttpActionResult PostUser([FromBody] UserDetailsDTO dto)
        {
            var hash = SecurePasswordHasher.Hash(dto.Password);
            dto.Password = hash;
            service.AddUser(dto);
            return Ok();
        }

    }
}

﻿using System.Collections.Generic;
using System.Linq;
using TaxiShareEntity;
using TaxiShareEntity.Domain;
using TaxiShareServer.Models;
using WebApi2.Models;

namespace WebApi2.Services
{
    public interface IPointService
    {
        IEnumerable<PointDetailsDTO> GetPoints();
        PointDetailsDTO GetPoint(long id);
        PointDetailsDTO Add(PointDetailsDTO newPoint);
        List<PointDetailsDTO> Search(SearchPointDTO searchPoint);
    }
    public class PointService : IPointService
    {
        private PointDetailsDTO Convert(Point point)
        {
            var list = new List<FareDTO>();
                foreach (var f in point.FareList)
                {
                    var fare = new FareDTO()
                    {
                        ID = f.ID,
                        From = f.From,
                        To = f.To,
                        When = f.When,
                        // UserListCount = f.UserList.Count
                    };
                    list.Add(fare);
                }
            return new PointDetailsDTO()
            {
                Name = point.Name,
                ID = point.ID,
                CoordX = point.CoordX,
                CoordY = point.CoordY,
                IsDefault = point.IsDefault,
                Votes = point.Votes,
                FareList = list
            };
        }

        private PointDetailsDTO Convert(Point point, EntityContext context)
        {
            var list = new List<FareDTO>();
                foreach (var f in point.FareList)
                {
                    var fare = new FareDTO()
                    {
                        ID = f.ID,
                        From = f.From,
                        To = f.To,
                        When = f.When,
                        // UserListCount = f.UserList.Count
                    };
                    list.Add(fare);
                }
            return new PointDetailsDTO()
            {
                Name = point.Name,
                ID = point.ID,
                CoordX = point.CoordX,
                CoordY = point.CoordY,
                IsDefault = point.IsDefault,
                Votes = point.Votes,
                FareList = list
            };
        }

        private Point Convert(PointDetailsDTO point)
        {
            return new Point()
            {
                Name = point.Name,
                ID = point.ID,
                CoordX = point.CoordX,
                CoordY = point.CoordY,
                IsDefault = point.IsDefault,
                Votes = point.Votes,
                FareList = new List<Fare>()
            };
        }

        public PointService()
        {
        }

        public IEnumerable<PointDetailsDTO> GetPoints()
        {
            IEnumerable<PointDetailsDTO> points;
            using (var e = new EntityContext())
            {
                var query = e.Points.OrderBy(x => x.ID).ToList();
                foreach (var q in query)
                {
                    if (q.FareList==null)
                        q.FareList = new List<Fare>();
                }
                points = query.ToList().Select(x => Convert(x, e));
            }
            return points;
        }
        public PointDetailsDTO GetPoint(long id)
        {
            PointDetailsDTO point = null;
            using (var e = new EntityContext())
            {
                var p = e.Points.FirstOrDefault(x => x.ID == id);
                if (p != null)
                    point = Convert(p, e);
            }
            return point;
        }

        public PointDetailsDTO Add(PointDetailsDTO newPoint)
        {
            if (newPoint == null)
                return null;
            Point point = Convert(newPoint);

            using (var e = new EntityContext())
            {
                e.Points.Add(point);
                e.SaveChanges();
            }

            return Convert(point);
        }

        public List<PointDetailsDTO> Search(SearchPointDTO searchPoint)
        {
            if (searchPoint == null)
                return null;
            List<Point> points;
            List<PointDetailsDTO> pointDtos = new List<PointDetailsDTO>();
            using (var e = new EntityContext())
            {
                IQueryable<Point> p = e.Points;

                if (searchPoint.ID != 0)
                    p = p.Where(x => x.ID == searchPoint.ID);
                if (!string.IsNullOrEmpty(searchPoint.Name))
                    p = p.Where(x => x.Name.Contains(searchPoint.Name));
                if (searchPoint.CoordX != 0)
                    p = p.Where(x => x.CoordX == searchPoint.CoordX);
                if (searchPoint.CoordY != 0)
                    p = p.Where(x => x.CoordY == searchPoint.CoordY);
                if (searchPoint.Votes != 0)
                    p = p.Where(x => x.Votes >= searchPoint.Votes);

                points = p.ToList();
            }
            using (var e = new EntityContext())
            {
                foreach (var p in points)
                {
                    var point = e.Points.SingleOrDefault(x => x.ID == p.ID);
                    pointDtos.Add(Convert(point));
                }
            }
            return pointDtos;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaxiShareServer.Models;
using TaxiShareEntity.Domain;

using AutoMapper;
using TaxiShareServer.Converters;
using TaxiShareEntity;
using System.Data.Entity.SqlServer;
using System.Net.Mail;

namespace TaxiShareServer.Services
{
    public interface IFareService
    {
        List<FareDTO> GetFares();
        List<FareDTO> SearchFare(FareDTO dto);
        FareDetailsDTO GetDetails(long ID);
        bool AddFare(FareNewDTO dto);
        Point ProcessPoint(double x, double y, EntityContext context);
        double CalculateCost(Point from, Point to);
        void JoinFare(FareNewDTO dto);
    }
    public class FareService : IFareService
    {

        public List<FareDTO> SearchFare(FareDTO dto)
        {
            using (var context = new EntityContext())
            {
                var timeTolerance = 30;
                var faresOnTime = context.Fares.Where(x => SqlFunctions.DateDiff("minute", x.When, dto.When) <= timeTolerance);
                bool from, to;
                var suitableFares = new List<Fare>();
                foreach (var fare in faresOnTime)
                {
                    from = false;
                    to = false;
                    if (fare.From == dto.From)
                        from = true;
                    if (fare.To == dto.To)
                        to = true;

                    foreach (var c in fare.Checkpoints)
                    {
                        if (c.Name == dto.From)
                            from = true;
                        else if (c.Name == dto.To)
                            to = true;
                    }
                    if (from && to)
                        suitableFares.Add(fare);
                }
                Mapper.CreateMap<Fare, FareDTO>();
                List<FareDTO> result = null;
                result = Mapper.Map<List<FareDTO>>(suitableFares);

                return result;

            }

        }

        public List<FareDTO> GetFares()
        {
            using (var context = new EntityContext())
            {
                var fares = context.Fares.ToList();
                Mapper.CreateMap<Fare, FareDTO>();
                List<FareDTO> result = Mapper.Map<List<FareDTO>>(fares);

                return result;

            }
        }
        public FareDetailsDTO GetDetails(long ID)
        {
            using (var context = new EntityContext())
            {
                var fare = context.Fares.FirstOrDefault(x => x.ID == ID);
                if (fare == null)
                    return null;

                FareDetailsDTO result = FareDetailsDTO2FareConverter.ConvertFromFare(fare);
                return result;

            }
        }

        public void SendSMS()
        {
            try
            {
                SmtpClient smtp = new SmtpClient();
                MailMessage message = new MailMessage();
                smtp.Host = "ipipi.com";
                message.From = new MailAddress(string.Format("{0}@ipipi.com", "aaa"));
                message.To.Add(string.Format("{0}@sms.ipipi.com", "aaa"));
                message.Subject = "Book a taxi";
                message.Body = "Please book a taxi now!";
                smtp.Send(message);
            }
            catch
            {
                Console.WriteLine("Bad phone number!!!");
            }
        }

        public bool AddFare(FareNewDTO dto)
        {
            using (var context = new EntityContext())
            {
                var fromPoint = ProcessPoint(dto.fromX, dto.fromY, context);
                var toPoint = ProcessPoint(dto.toX, dto.toY, context);
                var user = context.Users.FirstOrDefault(x => x.ID == dto.UserID);
                Random r = new Random();

                double taxiID = r.Next(context.Taxis.ToList().Count);
                var taxi = context.Taxis.FirstOrDefault(x => x.ID == taxiID);
                if (user == null || taxi == null)
                    return false;
                Fare fare = new Fare()
                {
                    From = String.IsNullOrEmpty(fromPoint.Name) ? "Default" : fromPoint.Name,
                    To = String.IsNullOrEmpty(toPoint.Name) ? "Default" : toPoint.Name,
                    Checkpoints = new List<Point>() { fromPoint, toPoint },
                    UserList = new List<User>() { user },
                    Cost = CalculateCost(fromPoint, toPoint),
                    When = dto.when,
                    Taxi = taxi
                };

                context.Fares.Add(fare);
                fromPoint.FareList.Add(fare);
                toPoint.FareList.Add(fare);
                user.Courses.Add(fare);
                taxi.FareList.Add(fare);
                context.SaveChanges();
                return true;

            }
        }
        public Point ProcessPoint(double x, double y, EntityContext context)
        {
            double pointToletance = 10;
            var processedPoint = context.Points.Where(z => Math.Abs(z.CoordX - x) <= pointToletance).FirstOrDefault(k => Math.Abs(k.CoordY - y) <= pointToletance);
            double votesToBeDefault = 15;

            if (processedPoint == null)
            {
                Point pointToAdd = new Point() { CoordX = x, CoordY = y, Name = "Unassigned", IsDefault = false, Votes = 1, FareList = new List<Fare>() };
                context.Points.Add(pointToAdd);
                context.SaveChanges();
                return pointToAdd;
            }
            processedPoint.IsDefault = processedPoint.Votes++ > votesToBeDefault ? true : false;
            return processedPoint;
        }
        public double CalculateCost(Point from, Point to)
        {
            double cost4km = 10;
            return cost4km * Math.Sqrt((from.CoordX - to.CoordX) * (from.CoordX - to.CoordX) + (from.CoordY - to.CoordY) * (from.CoordY - to.CoordY));
        }

        public void JoinFare(FareNewDTO dto)
        {
            using (var context = new EntityContext())
            {
                var fare = context.Fares.FirstOrDefault(x => x.ID == dto.FareID);
                var user = context.Users.FirstOrDefault(x => x.ID == dto.UserID);
                var fromPoint = ProcessPoint(dto.fromX, dto.fromY, context);
                var toPoint = ProcessPoint(dto.toX, dto.toY, context);

                if (fare == null || user == null)
                    return;

                if (fare.Checkpoints.FirstOrDefault(x => x.ID == fromPoint.ID) == null)
                    fare.Checkpoints.Add(fromPoint);

                if (fare.Checkpoints.FirstOrDefault(x => x.ID == toPoint.ID) == null)
                    fare.Checkpoints.Add(toPoint);
                user.Courses.Add(fare);
                fare.UserList.Add(user);
                SendSMS();

                //co z kosztami jak ktos sie dolacza ?/
                context.SaveChanges();
            }
        }
    }
}
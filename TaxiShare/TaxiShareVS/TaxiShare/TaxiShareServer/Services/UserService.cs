﻿using System.Collections.Generic;
using System.Linq;
using TaxiShareEntity;
using TaxiShareServer.Models;
using AutoMapper;
using TaxiShareEntity.Domain;
using TaxiShareServer.Converters;

namespace TaxiShareServer.Services
{
    public interface IUserService
    {
        List<UserDTO> GetUsers();
        UserDetailsDTO GetDetails(long ID);
        void AddUser(UserDetailsDTO dto);
        long GetId(string login);
    }
    public class UserService:IUserService
    {
       

        public List<UserDTO> GetUsers()
        {
            using (var context = new EntityContext())
            {
                var users = context.Users;
                Mapper.CreateMap<User, UserDTO>();
                List<UserDTO> result = Mapper.Map<List<UserDTO>>(users);

                return result;
            }
        }
        public UserDetailsDTO GetDetails(long ID)
        {
            using (var context = new EntityContext())
            {
                var user = context.Users.FirstOrDefault(x => x.ID == ID);
                if (user == null)
                    return null;

                UserDetailsDTO result = UserDetailsDTO2User.ConverFromUser(user);
                return result;

            }
        }
        public void AddUser(UserDetailsDTO dto)
        {
            using (var context = new EntityContext())
            {          
                Mapper.CreateMap<UserDetailsDTO, User>();
                User user = Mapper.Map<User>(dto);
                user.Courses = new List<Fare>();
                context.Users.Add(user);
                context.SaveChanges();
            }
        }

        public long GetId(string login)
        {
            using (var context = new EntityContext())
            {
                var user = context.Users.FirstOrDefault(x => x.Login == login);
                long result = user == null ? 0 : user.ID;
                return result;
            }
        }
    }
}
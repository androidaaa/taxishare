﻿using System.Collections.Generic;
using System.Linq;
using TaxiShareEntity;
using TaxiShareEntity.Domain;
using TaxiShareServer.Models;
using WebApi2.Models;

namespace TaxiShareApp.Services
{
    public interface ITaxiService
    {
        IEnumerable<TaxiDetailsDTO> GetTaxis();
        TaxiDetailsDTO GetTaxi(long id);
        TaxiDetailsDTO Add(NewTaxiDTO newTaxi);
        List<TaxiDetailsDTO> Search(SearchTaxiDTO searchTaxi);
    }

    public class TaxiService : ITaxiService
    {
        public TaxiDetailsDTO Convert(Taxi taxi)
        {
            
         //   AutoMapper.Mapper.CreateMap<Fare, FareDTO>();
           // List<FareDTO> list = AutoMapper.Mapper.Map<List<FareDTO>>(taxi.FareList);

            List<FareDTO> list = new List<FareDTO>();
            foreach (var f in taxi.FareList)
            {
                var fare = new FareDTO()
                {
                    ID = f.ID,
                    From = f.From,
                    To = f.To,
                    When = f.When,
                    UserListCount = f.UserList.Count
                };
                list.Add(fare);
            }

            return new TaxiDetailsDTO()
            {
                ID = taxi.ID,
                DriverName = taxi.DriverName,
                DriverSurname = taxi.DriverSurname,
                PhoneNumber = taxi.PhoneNumber,
                FareList = list
            };
        }

        public TaxiDetailsDTO Convert(Taxi taxi, EntityContext context)
        {

            //   AutoMapper.Mapper.CreateMap<Fare, FareDTO>();
            // List<FareDTO> list = AutoMapper.Mapper.Map<List<FareDTO>>(taxi.FareList);

            List<FareDTO> list = new List<FareDTO>();
            foreach (var f in taxi.FareList)
            {
                var fare = new FareDTO()
                {
                    ID = f.ID,
                    From = f.From,
                    To = f.To,
                    When = f.When,
                 //   UserListCount = f.UserList.Count
                };
                list.Add(fare);
            }

            return new TaxiDetailsDTO()
            {
                ID = taxi.ID,
                DriverName = taxi.DriverName,
                DriverSurname = taxi.DriverSurname,
                PhoneNumber = taxi.PhoneNumber,
                FareList = list
            };
        }

        public Taxi Convert(NewTaxiDTO taxi)
        {
            return new Taxi()
            {
                ID = taxi.ID,
                DriverName = taxi.DriverName,
                DriverSurname = taxi.DriverSurname,
                PhoneNumber = taxi.Telephone,
                FareList = new List<Fare>() { }
            };
        }
        public TaxiService()
        {
        }

        public IEnumerable<TaxiDetailsDTO> GetTaxis()
        {
            IEnumerable<TaxiDetailsDTO> taxis;
            using (var e = new EntityContext())
            {
                var query = e.Taxis.OrderBy(x => x.ID).ToList();
                foreach (var q in query)
                {
                    if(q.FareList ==null)
                        q.FareList = new List<Fare>();
                }
                taxis = query.ToList().Select(x => Convert(x, e));
            }
            return taxis;
        }

        public TaxiDetailsDTO GetTaxi(long id)
        {
            TaxiDetailsDTO taxi = null;
            using (var e = new EntityContext())
            {
                var t = e.Taxis.FirstOrDefault(x => x.ID == id);
                if (t != null)
                    taxi = Convert(t, e);
            }
            return taxi;
        }

        public TaxiDetailsDTO Add(NewTaxiDTO newTaxi)
        {
            if (newTaxi == null)
                return null;
            Taxi taxi = Convert(newTaxi);

            using (var e = new EntityContext())
            {
                e.Taxis.Add(taxi);
                e.SaveChanges();
            }

            return Convert(taxi);
        }

        public List<TaxiDetailsDTO> Search(SearchTaxiDTO searchTaxi)
        {
            if (searchTaxi == null)
                return null;
            List<Taxi> taxis;
            List<TaxiDetailsDTO> taxiDtos = new List<TaxiDetailsDTO>();
            using (var e = new EntityContext())
            {
                IQueryable<Taxi> t = e.Taxis;

                if (searchTaxi.TaxiID != 0)
                    t = t.Where(x => x.ID == searchTaxi.TaxiID);
                if (!string.IsNullOrEmpty(searchTaxi.DriverName))
                    t = t.Where(x => x.DriverName.Contains(searchTaxi.DriverName));
                if (!string.IsNullOrEmpty(searchTaxi.DriverSurname))
                    t = t.Where(x => x.DriverSurname.Contains(searchTaxi.DriverSurname));

                taxis = t.ToList();
            }
            using (var e = new EntityContext())
            {
                foreach (var t in taxis)
                {
                    var taxi = e.Taxis.SingleOrDefault(x => x.ID == t.ID);
                    taxiDtos.Add(Convert(taxi));
                }
            }
            return taxiDtos;
        }
    }
}
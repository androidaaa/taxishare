﻿using AutoMapper;
using System.Collections.Generic;
using TaxiShareEntity.Domain;
using TaxiShareServer.Models;

namespace TaxiShareServer.Converters
{
    public static class FareDetailsDTO2FareConverter
    {
        public static FareDetailsDTO ConvertFromFare(Fare fare)
        {
            FareDetailsDTO dto = new FareDetailsDTO();
            Mapper.CreateMap<User, UserDTO>();
            List<UserDTO> users = Mapper.Map<List<UserDTO>>(fare.UserList);
            Mapper.CreateMap<Point, PointDTO>();
            List<PointDTO> points = Mapper.Map<List<PointDTO>>(fare.Checkpoints);
            Mapper.CreateMap<Taxi, TaxiDTO>();
            TaxiDTO taxi = Mapper.Map<TaxiDTO>(fare.Taxi);

            dto.From = fare.From;
            dto.To = fare.To;
            dto.When = fare.When;
            dto.Cost = fare.Cost;
            dto.Checkpoints = points;
            dto.UserList = users;
            dto.Taxi = taxi;
            dto.ID = fare.ID;
            return dto;

        }
    }
}
﻿using AutoMapper;
using System.Collections.Generic;
using TaxiShareEntity.Domain;
using TaxiShareServer.Models;

namespace TaxiShareServer.Converters
{
    public static class UserDetailsDTO2User
    {
        public static UserDetailsDTO ConverFromUser(User user)
        {
            UserDetailsDTO userDto = new UserDetailsDTO();
            userDto.Gender = user.Gender;
            userDto.Login = user.Login;
            userDto.Password = user.Password;
            userDto.ID = user.ID;
            userDto.Name = user.Name;
            userDto.Surname = user.Surname;

            Mapper.CreateMap<Fare, FareDTO>();
            List<FareDTO> courses = Mapper.Map<List<FareDTO>>(user.Courses);
            userDto.Courses = courses;
            return userDto;
        }
    }
}
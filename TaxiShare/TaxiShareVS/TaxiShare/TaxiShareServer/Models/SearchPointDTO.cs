﻿namespace WebApi2.Models
{
    public class SearchPointDTO
    {
        public string Name { get; set; }
        public long ID { get; set; }
        public int CoordX { get; set; }
        public int CoordY { get; set; }
        public int Votes { get; set; }
    }
}
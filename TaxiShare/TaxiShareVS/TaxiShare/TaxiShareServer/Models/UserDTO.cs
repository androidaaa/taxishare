﻿namespace TaxiShareServer.Models
{
    public class UserDTO
    {
        public long ID { get; set; }
        public string Login { get; set; }
    }
}
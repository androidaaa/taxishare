﻿using System.Collections.Generic;

namespace TaxiShareServer.Models
{
    public class TaxiDetailsDTO
    {
        public long ID { get; set; }
        public string DriverName { get; set; }

        public string DriverSurname { get; set; }

        public string PhoneNumber { get; set; }
        public ICollection<FareDTO> FareList { get; set; }
    }
}
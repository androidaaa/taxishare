﻿

namespace TaxiShareServer.Models.ExternalApiModels
{
    public class StationDTO
    {
        public double CoordX { get; set; }
        public double CoordY { get; set; }
        public string Name { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace TaxiShareServer.Models
{
    public class UserDetailsDTO
    {
        public  long ID { get; set; }
        public  string Name { get; set; }

        public  string Surname { get; set; }
       
        public  string Login { get; set; }
      
        public  string Password { get; set; }
       
        public  bool Gender { get; set; }

        public  ICollection<FareDTO> Courses { get; set; }
    }
}
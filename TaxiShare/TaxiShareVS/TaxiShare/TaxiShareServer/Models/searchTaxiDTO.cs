﻿namespace WebApi2.Models
{
    public class SearchTaxiDTO
    {
        public long TaxiID { get; set; }
        public string DriverName { get; set; }
        public string DriverSurname { get; set; }
    }
}
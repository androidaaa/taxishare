﻿namespace TaxiShareServer.Models
{
    public class TaxiDTO
    {

        public long ID { get; set; }
        public string DriverName { get; set; }

        public string DriverSurname { get; set; }

        public string PhoneNumber { get; set; }
    }
}
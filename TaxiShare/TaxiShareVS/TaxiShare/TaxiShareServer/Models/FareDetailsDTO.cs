﻿using System;
using System.Collections.Generic;

namespace TaxiShareServer.Models
{
    public class FareDetailsDTO
    {
        public string From { get; set; }

        public string To { get; set; }

        public DateTime When { get; set; }

        public double Cost { get; set; }

        public List<UserDTO> UserList { get; set; }

        public List<PointDTO> Checkpoints { get; set; }

        public TaxiDTO Taxi { get; set; }

        public long ID { get; set; }

    }
}
﻿namespace TaxiShareServer.Models
{
    public class PointDTO
    {
        public long ID { get; set; }

        public string Name { get; set; }

        public double CoordX { get; set; }

        public double CoordY { get; set; }
    }
}
﻿using System;

namespace TaxiShareServer.Models
{
    public class FareNewDTO
    {
        public long UserID { get; set; }
        public long FareID { get; set; }
         public double fromX { get; set; }
        public double fromY { get; set; }
        public double toX { get; set; }
        public double toY {  get; set; }
        public DateTime when { get; set; }

    }
}
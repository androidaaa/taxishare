﻿using System.Collections.Generic;

namespace TaxiShareServer.Models
{
    public class PointDetailsDTO
    {
        public long ID { get; set; }
        public string Name { get; set; }

        public double CoordX { get; set; }

        public double CoordY { get; set; }
        public int Votes { get; set; }
        public bool IsDefault { get; set; }

        public ICollection<FareDTO> FareList { get; set; }
    }
}
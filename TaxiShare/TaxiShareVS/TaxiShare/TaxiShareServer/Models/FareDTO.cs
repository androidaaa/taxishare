﻿using System;

namespace TaxiShareServer.Models
{
    public class FareDTO
    {
        public long ID { get; set; }
        public string From { get; set; }

        public string To { get; set; }

        public DateTime When { get; set; }

        public int UserListCount { get; set; }


    }
}
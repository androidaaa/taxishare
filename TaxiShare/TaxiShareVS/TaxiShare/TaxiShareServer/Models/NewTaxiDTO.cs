﻿namespace WebApi2.Models
{
    public class NewTaxiDTO
    {
        public long ID { get; set; }
        public string DriverName { get; set; }
        public string DriverSurname { get; set; }
        public string Telephone { get; set; }
    }
}
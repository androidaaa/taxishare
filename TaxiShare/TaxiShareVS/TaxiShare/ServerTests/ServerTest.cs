﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using TaxiShareServer.Services;
using TaxiShareServer.Controllers;
using TaxiShareServer.Models;
using System.Collections.Generic;
using System.Web.Http;
using TaxiShareApp.Services;
using WebApi2.Models;
using WebApi2.Controllers;
using WebApi2.Services;

namespace ServerTests
{
    [TestClass]
    public class ServerTest
    {
        [TestMethod]
        public void GetAllFaresTestMethod()
        {
            var fareServiceMock = MockRepository.GenerateMock<IFareService>();

            fareServiceMock.Expect(x => x.GetFares());

            var controller = new FareController(fareServiceMock);
            var result = controller.GetFares();

            Assert.IsNotNull(result);
            fareServiceMock.VerifyAllExpectations();
        }

        [TestMethod]
        public void AddFareTestMethod()
        {
            var fareServiceMock = MockRepository.GenerateMock<IFareService>();
            var newFare = new FareNewDTO
            {
                FareID = 5,
                fromX = 48.9,
                fromY = 34.6,
                toX = 78.0,
                toY = 99.12,
                UserID = 1,
                when = DateTime.Now
            };

            fareServiceMock.Stub(x => x.AddFare(newFare)).Return(true);

            var fareController = new FareController(fareServiceMock);
            fareController.PostFare(newFare);

            fareServiceMock.AssertWasCalled(x => x.AddFare(newFare));
        }

        [TestMethod]
        public void SearchFareMethod()
        {
            var fareServiceMock = MockRepository.GenerateMock<IFareService>();
            var fare = new FareDTO
            {
                From = "Metro Politechnika",
                To = "Metro Kabaty",
                ID = 2,
                UserListCount = 5,
                When = DateTime.Now
            };

            fareServiceMock.Stub(x => x.SearchFare(fare)).Return(null);
            var fareController = new FareController(fareServiceMock);
            var result = fareController.SearchFare(fare) as List<FareDTO>;

            Assert.IsNull(result);
        }

        [TestMethod]
        public void GetUserByIdMethod()
        {
            var userServiceMock = MockRepository.GenerateMock<IUserService>();
            var id = 2;

            userServiceMock.Expect(x => x.GetDetails(id));

            var userController = new UserController(userServiceMock);
            var result = userController.Get(id);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetUserDetailsMethod()
        {
            var userServiceMock = MockRepository.GenerateMock<IUserService>();
            var id = 3;

            var user = new UserDetailsDTO
            {
                ID = 3,
                Name = "Dave",
                Surname = "User",
                Login = "charlie_izdcvpm_user@tfbnm.net",
                Gender = true
            };

            userServiceMock.Stub(x => x.GetDetails(id)).Return(user);

            var userController = new UserController(userServiceMock);
            var result = userController.Get(id);

            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public void SearchTaxiMethod()
        {
            var taxiServiceMock = MockRepository.GenerateMock<ITaxiService>();
            var taxi = new SearchTaxiDTO
            {
                DriverName = "Marcin"
            };

            taxiServiceMock.Stub(x => x.Search(taxi));

            var taxiController = new TaxiController(taxiServiceMock);
            var result = taxiController.Search(taxi);

            Assert.IsNotNull(taxi);
        }

        [TestMethod]
        public void GetPointsMethod()
        {
            var pointServiceMock = MockRepository.GenerateMock<IPointService>();

            pointServiceMock.Expect(x => x.GetPoints());

            var controller = new PointController(pointServiceMock);
            var result = controller.Get();

            Assert.IsNotNull(result);
            pointServiceMock.VerifyAllExpectations();
        }

        [TestMethod]
        public void AddPointMethod()
        {
            var pointServiceMock = MockRepository.GenerateMock<IPointService>();
            var newPoint = new PointDetailsDTO
            {
                CoordX = 27.03,
                CoordY = 52.98,
                Name = "Kino Femina"
            };

            pointServiceMock.Stub(x => x.Add(newPoint)).Return(newPoint);

            var pointController = new PointController(pointServiceMock);
            pointController.Post(newPoint);

            pointServiceMock.AssertWasCalled(x => x.Add(newPoint));
        }

        [TestMethod]
        public void GetTaxiMethod()
        {
            var taxiServiceMock = MockRepository.GenerateMock<ITaxiService>();
            var id = 2;

            taxiServiceMock.Expect(x => x.GetTaxi(id));

            var taxiController = new TaxiController(taxiServiceMock);
            var result = taxiController.Get(id);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void SearchPointMethod()
        {
            var pointServiceMock = MockRepository.GenerateMock<IPointService>();

            var searchPoint = new SearchPointDTO
            {
                CoordX = 198
            };

            pointServiceMock.Stub(x => x.Search(searchPoint)).Return(null);

            var pointController = new PointController(pointServiceMock);
            pointController.Search(searchPoint);

            pointServiceMock.VerifyAllExpectations();
        }
    }
}

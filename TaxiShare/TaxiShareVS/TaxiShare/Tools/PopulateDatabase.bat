@ECHO OFF
ECHO ========Populate Developer Database========
ECHO.
ECHO 0)Clear database
sqlcmd -S "(local)\SQLEXPRESS" -i ".\Scripts\ClearDatabase.sql"
ECHO.
ECHO 1)Populate Taxis
ECHO.
sqlcmd -S "(local)\SQLEXPRESS"  -i ".\Scripts\PopulateTaxis.sql"
ECHO.
ECHO 2)Populate Points
ECHO.
sqlcmd -S "(local)\SQLEXPRESS"  -i ".\Scripts\PopulatePoints.sql"
ECHO.
ECHO 3)Populate Fares
ECHO.
sqlcmd -S "(local)\SQLEXPRESS"  -i ".\Scripts\PopulateFares.sql"
ECHO.
ECHO 4)Populate PointFares
ECHO.
sqlcmd -S "(local)\SQLEXPRESS"  -i ".\Scripts\PopulatePointsFares.sql"
ECHO.
ECHO 5)Populate Users
ECHO.
sqlcmd -S "(local)\SQLEXPRESS"  -i ".\Scripts\PopulateUsers.sql"
ECHO 6)Populate UsersFares
ECHO.
sqlcmd -S "(local)\SQLEXPRESS"  -i ".\Scripts\PopulateUsersFares.sql" 
pause
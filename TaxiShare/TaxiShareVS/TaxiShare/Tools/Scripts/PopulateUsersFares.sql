USE [TaxiShareData]
GO


DECLARE @FareId BIGINT
DECLARE @UserId BIGINT

SET @FareId = (SELECT TOP 1 Id FROM Fares ORDER BY [Id])
SET @UserId = (SELECT TOP 1 Id FROM Users ORDER BY [Id])
INSERT INTO UserFares(User_ID,Fare_ID)
VALUES
(@UserId,@FareId),
(@UserId+1,@FareId),
(@UserId+2,@FareId+1),
(@UserId,@FareId+1),
(@UserId+3,@FareId+2),
(@UserId+4,@FareId),
(@UserId+1,@FareId+3),
(@UserId,@FareId+3);


USE TaxiShareData
GO

EXEC sp_MSforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL'

EXEC sp_MSforeachtable 'IF OBJECT_ID(''?'') NOT IN (
                                                    ISNULL(OBJECT_ID(''[dbo].[__MigrationHistory]''),0)
                                                   )
                        DELETE FROM ?'


EXEC sp_MSforeachtable 'ALTER TABLE ? CHECK CONSTRAINT ALL'
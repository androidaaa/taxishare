USE [TaxiShareData]
GO

INSERT INTO [TaxiShareData].[dbo].[Points]([Name], [CoordX], [CoordY], [Votes], [IsDefault]) 
VALUES
('Metro Politechnika', 152, 15, 5000, 1), 
('Metro Centrum', 345, 120, 6578, 1),
('Teatr Wielki', 123, 0, 3000, 1),
('Metro Kabaty', 345, 12, 2567, 1),
('Hotel Hilton', 52, 159, 100, 0),
('Hotel Marriot', 56, 1, 45, 0),
('Centrum Handlowe Arkadia', 33, 123, 345, 0),
('Pa�ac Prezydencki', 23, 13, 370, 0),
('Stare Miasto', 56, 130, 789, 1);


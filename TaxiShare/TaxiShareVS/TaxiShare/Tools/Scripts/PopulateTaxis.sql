USE [TaxiShareData]
GO

IF EXISTS (SELECT TOP 1 [Id] FROM [Taxis])
BEGIN
	RETURN;
END

INSERT INTO [TaxiShareData].[dbo].[Taxis]([DriverName], [DriverSurname], [PhoneNumber]) 
VALUES
('Jan', 'Nowak', '(22) 521-882-198'), 
('Marek', 'Kowalski', '(22) 321-122-195'),
('Krzysztof', 'Malina', '(22) 547-228-456'),
('Leszek', 'Marcinkowski', '(22) 145-567-199'),
('Marcin', 'Malecki', '(22) 525-852-345'),
('Jerzy', 'Kowalczuk', '(22) 678-445-678'),
('Katarzyna', 'Je�yna', '(22) 334-190-190'),
('Magdalena', 'Nowakowska', '(22) 345-123-222')

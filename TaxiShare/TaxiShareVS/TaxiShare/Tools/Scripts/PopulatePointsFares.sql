USE [TaxiShareData]
GO

IF EXISTS (SELECT TOP 1 [Point_ID] FROM [PointFares])
BEGIN
	RETURN;
END

DECLARE @FareId BIGINT
DECLARE @PointId BIGINT

SET @FareId = (SELECT TOP 1 Id FROM [Fares] ORDER BY [Id])
SET @PointId = (SELECT TOP 1 Id FROM [Points] ORDER BY [Id])

INSERT INTO [TaxiShareData].[dbo].[PointFares]([Point_ID], [Fare_ID]) 
VALUES
(@PointId, @FareId), 
(@PointId, @FareId+1),
(@PointId+1, @FareId),
(@PointId+2, @FareId+1),
(@PointId+3, @FareId+2);


USE [TaxiShareData]
GO

IF EXISTS (SELECT TOP 1 [Id] FROM [Fares])
BEGIN
	RETURN;
END

DECLARE @TaxiId BIGINT

SET @TaxiId = (SELECT TOP 1 Id FROM [Taxis] ORDER BY [Id])

INSERT INTO [TaxiShareData].[dbo].[Fares]([From], [To], [When], [Cost], [Taxi_ID]) 
VALUES
('Metro Kabaty', 'Metro Centrum', '01/01/2016 23:23:00', 25.80, @TaxiId), 
('Teatr Wielki', 'Metro Kabaty', '12/02/2016 15:00:00', 65.20, @TaxiId),
('Hotel Hilton', 'Hotel Marriot', '05/05/2016 20:20:00', 33.40, @TaxiId),
('Centrum Handlowe Arkadia', 'Metro Centrum', '06/05/2016 17:30:00', 89.30, @TaxiId),
('Metro Politechnika', 'Hotel Marriot', '07/06/2016 12:15:00', 45.70, @TaxiId),
('Metro Politechnika', 'Satre Miasto', '08/09/2016 19:15:00', 25.90, @TaxiId);
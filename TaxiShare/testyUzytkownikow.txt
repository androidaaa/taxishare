Testy u�ytkowe u�ytkownik�w.

Opis reakcji testuj�cych w trakcie interakcji z aplikacj�:
	
Uwagi u�ytkownik nr 1.
	Niewidoczne litery w niekt�rych widokach.
Zmiany poczynione:
	Zmiana przezroczysto�ci t�a w niekt�rych widokach np: listFoundFares oraz zmiana koloru i grubo�ci czcionki

Uwagi u�ytkownik nr 2.
	Nie wygodne korzystanie z wpisywania miejsca docelowego przy wyborze nowego przejazdu
Zmiany poczynione:
	Zamiana z textBox na comboBox dla wylistowania miejsc

Uwagi u�ytkownik nr 3.
	Brak intuincyjnego przej�cia w widokach s�u��cych do tworzenia przejazd�w, a widokach s�u��cych 
do znajdowania gotowych przejazd�w
Zmiany poczynione:
	U�ytkownik po wypisaniu informacji dotycz�cych przejazdu ma mo�liwo�� wyboru utworzenia nowego kursu b�d� 
przy��czenia si� do gotowych kurs�w.

Uwaga uzytkownik nr 4
	Brak sensu w wpisywaniu daty przy zamawianiu taks�wki
Zmiany poczynione:
	U�ytkownik ma mo�liowo�� jedynie wybrania godziny, a wi�c taks�wka jest zamawiana w okresie 24 godzin w prz�d.


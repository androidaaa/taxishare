package com.taxishare;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.taxishare.Infrastructure.ApplicationComponent;
import com.taxishare.Infrastructure.ApplicationModule;
import com.taxishare.Infrastructure.DaggerApplicationComponent;
import com.taxishare.Infrastructure.RestServicesModule;


public class MyApplication extends Application {

    private static String baseUrl = "http://taxishare3.azurewebsites.net/";

    private ApplicationComponent servicesComponent;
    private static Context context;
    
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    @Override
    public void onCreate() {
        super.onCreate();
        MyApplication.context = getApplicationContext();
        servicesComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .restServicesModule(new RestServicesModule(baseUrl))
                .build();
    }

    public ApplicationComponent getServicesComponent() {
        return servicesComponent;
    }

    public static Context getContext() {
        return MyApplication.context;
    }

}


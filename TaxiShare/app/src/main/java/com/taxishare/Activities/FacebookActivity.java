package com.taxishare.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.LoggingBehavior;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.taxishare.Infrastructure.CustomCallback;
import com.taxishare.MyApplication;
import com.taxishare.R;
import com.taxishare.Services.UserService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import javax.inject.Inject;

import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;

public class FacebookActivity extends AppCompatActivity {

    private LoginButton loginButton;
    private long UserID;
    private String FEmail;
    CallbackManager callbackManager;
    Context context;
    @Inject UserService userService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context=this;
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        FacebookSdk.addLoggingBehavior(LoggingBehavior.REQUESTS);
        setContentView(R.layout.activity_facebook);

        ((MyApplication) getApplication()).getServicesComponent().inject(this);
         callbackManager = CallbackManager.Factory.create(); 
            loginButton=(LoginButton)findViewById(R.id.connectWithFbButton);
             loginButton.setReadPermissions(Arrays.asList(
                     "public_profile", "email", "user_birthday", "user_friends"));





        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("LoginActivity", response.toString());

                                        try {
                                            String Name = object.getString("name");

                                            FEmail = object.getString("email");
                                            Log.v("Email = ", " " + FEmail);
                                           // Toast.makeText(getApplicationContext(), "Email " + FEmail, Toast.LENGTH_LONG).show();
                                            Toast.makeText(getApplicationContext(), "Welcome " + Name, Toast.LENGTH_LONG).show();



                                            Call<Long> call = userService.getId(FEmail);
                                            call.enqueue(new CustomCallback<Long>(context) {

                                                @Override
                                                public void onSuccess(Long model) {
                                                    test(model);
                                                }

                                                @Override
                                                public void onFailure(Throwable t) {
                                                    Log.d("error", t.toString());
                                                }

                                                @Override
                                                public void onResponse(Response<Long> response, Retrofit retrofit) {
                                                    test(response.body());
                                                }
                                            });




                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();


                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.d("EXCEPTION",exception.toString());
                    }
                });





    }
//

    private void test(Long model)
    {
        if (model!=null && model > 0) {
            UserID=model;
            Intent intent = new Intent(FacebookActivity.this, MapActivity.class);
            intent.putExtra("UserID", UserID);
            startActivity(intent);
        }
        else {
            UserID=-1;
            Intent intent = new Intent(FacebookActivity.this, RegisterActivity.class);
            startActivity(intent);

        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        Log.d("DATA", data.toString());
        Toast.makeText(this,data.toString(),Toast.LENGTH_LONG);
    }
    @Override
    protected void onResume() {
        super.onResume();

        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        AppEventsLogger.deactivateApp(this);
    }

}

package com.taxishare.Activities;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TimePicker;
import android.widget.Toast;

import com.taxishare.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateTimeActivity extends AppCompatActivity {
    private CalendarView calendar;
    private EditText date, time;
    private String currentDate;
    private Date dateChosen;
    private TimePicker timePicker;

    //do dodania do popUpWindow
    private PopupWindow pw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_time);

        date = (EditText) findViewById(R.id.Date);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog(v);

            }
        });

        time = (EditText) findViewById(R.id.Time);
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeDialog(v);

            }
        });
    }

    public void setCalendar()
    {
        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                SimpleDateFormat ss = new SimpleDateFormat("MM-dd-yyyy");
                dateChosen = new Date(year, month, dayOfMonth);
                currentDate = ss.format(dateChosen);
                Toast.makeText(getApplicationContext(), currentDate,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        date.setText(sdf.format(dateChosen));
    }

    public void showDateDialog(View view)
    {
        AlertDialog.Builder builder2 = new AlertDialog.Builder(view.getContext());
        builder2.setTitle("Choose a date");
        calendar = new CalendarView(view.getContext());
        setCalendar();
        builder2.setView(calendar);
        calendar.animate();
        builder2.setMessage("What do you want to do?");
        builder2.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int j) {
                updateLabel();
            }
        });
        builder2.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int j) {
            }
        });
        builder2.create().show();
    }

    public void setTimePicker()
    {
        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {

            @Override
            public void onTimeChanged(TimePicker timePicker, int selectedHour, int selectedMinute) {
                time.setText(selectedHour + ":" + selectedMinute);
            }
        });
    }

    public void showTimeDialog(View view)
    {
        AlertDialog.Builder builder2 = new AlertDialog.Builder(view.getContext());
        builder2.setTitle("Choose time");
        timePicker= new TimePicker(view.getContext());
        setTimePicker();
        builder2.setView(timePicker);
        timePicker.animate();
        builder2.setMessage("At what time do you want to go?");
        builder2.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int j) {
            }
        });
        builder2.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int j) {
            }
        });
        builder2.create().show();
    }

    public void Save(View view) {
        if(date.getText()== null || time.getText() == null)
            Toast.makeText(getApplicationContext(), "Error! Fill the gaps.",
                    Toast.LENGTH_SHORT).show();

        Toast.makeText(getApplicationContext(), "Your choice was saved.",
                Toast.LENGTH_SHORT).show();

        checkPopUp();
        //Intent i=new Intent(DateTimeActivity.this,FoundCoursesActivity.class);
        //startActivity(i);
    }

    public void btnClick(View v){
        NumberFormat formatter = new DecimalFormat("00");
        Toast.makeText(getBaseContext(), "Time selected:" +timePicker.getCurrentHour() +
                ":" + formatter.format(timePicker.getCurrentMinute()),Toast.LENGTH_SHORT).show();
    }


    private void checkPopUp(){
        try {
            //We need to get the instance of the LayoutInflater, use the context of this activity
            LayoutInflater inflater = (LayoutInflater) getBaseContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //Inflate the view from a predefined XML layout
            View layout = inflater.inflate(R.layout.now_popup_layout,null, false);



            // create a 300px width and 470px height PopupWindow
            pw = new PopupWindow(layout, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT, true);
            // display the popup in the center
            pw.showAtLocation(layout, Gravity.CENTER, 0, 0);

            pw.setOutsideTouchable(true);
            pw.setFocusable(true);
            pw.update();

            Button cancelButton = (Button) layout.findViewById(R.id.cancel_button);
            Button submitButton = (Button) layout.findViewById(R.id.submit_button);




            cancelButton.setOnClickListener(new Button.OnClickListener(){
                public void onClick(View v) {
                    pw.dismiss();
                }
            });

            submitButton.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {

                    final ProgressDialog progressDialog = new ProgressDialog(DateTimeActivity.this,
                            R.style.AppTheme_Dark_Dialog);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage("Looking for a taxi...");
                    progressDialog.show();

                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    progressDialog.dismiss();
                                    Toast.makeText(getBaseContext(), "A Taxi is coming!", Toast.LENGTH_LONG).show();
                                }
                            }, 3000);

                    pw.dismiss();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

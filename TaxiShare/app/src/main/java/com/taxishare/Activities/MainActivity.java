package com.taxishare.Activities;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.taxishare.R;



public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        final ProgressBar mProgress= (ProgressBar) findViewById(R.id.progressBar);
        mProgress.setVisibility(View.VISIBLE);



        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                final Intent mainIntent = new Intent(MainActivity.this, WelcomeActivity.class);
                MainActivity.this.startActivity(mainIntent);
                MainActivity.this.finish();
            }
        }, 5000);


    }

    public void SetDimensions(ImageView taxiLogo)
    {
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.logo2);
        int imageWidth = bitmap.getWidth();
        int newHeight = getApplicationContext().getResources().getDisplayMetrics().heightPixels;


        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap,(int)(1.2*imageWidth), (int)(1.1*newHeight), true);
        taxiLogo.setImageBitmap(resizedBitmap);

    }
}

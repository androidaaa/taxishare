package com.taxishare.Activities;

import android.Manifest;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.okhttp.ResponseBody;
import com.taxishare.DataProvider;
import com.taxishare.Infrastructure.CustomCallback;
import com.taxishare.Models.Fare;
import com.taxishare.Models.FareNew;
import com.taxishare.Models.Point;
import com.taxishare.Models.UserDetails;
import com.taxishare.MyApplication;
import com.taxishare.R;
import com.taxishare.Services.FareService;
import com.taxishare.Services.PointService;
import com.taxishare.Services.UserService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback,AdapterView.OnItemSelectedListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    public static final String TAG = MapActivity.class.getSimpleName();
    private LocationRequest mLocationRequest;
    private ViewFlipper viewFlipper;
    private float lastX;
    private ImageView newCourseBtn;

    private PopupWindow pw;

  //  private ImageView searchCourseBtn;
    private Fare newFare=null;
    private Button bookBtn;
    private Button joinBtn;
    private Context context;
    private Spinner spinner;
    private EditText destinationEdit;

    static final LatLng PALACEOFCULTURE = new LatLng(52.13, 21);
    ProgressDialog progressDialog=null;

    private List<Point> toPoints;
    private List<String> toPointsNames;
    private Point locationPoint;
    final String ekspres = "Ekspres";

    private Location location;
    private LatLng toLocation;
    private long UserID;
    private UserDetails userDetails;
    @Inject FareService fareService;
    @Inject PointService pointService;
    @Inject UserService userService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ((MyApplication) getApplication()).getServicesComponent().inject(this);
        newFare=new Fare();
        viewFlipper= (ViewFlipper) findViewById(R.id.viewFlipper);
        newCourseBtn=(ImageView)findViewById(R.id.addCourseFlipBtn);
        bookBtn=(Button)findViewById(R.id.btn_book);
        joinBtn=(Button)findViewById(R.id.btn_join);
        spinner=(Spinner)findViewById(R.id.destEdit);
        newCourseBtn.setOnClickListener(newCourseBtnListener);
        joinBtn.setOnClickListener(newCourseBtnListener);
        bookBtn.setOnClickListener(bookBtnListener);
        destinationEdit=(EditText)findViewById(R.id.destinationEdit);

        destinationEdit.setOnEditorActionListener(destinationEditListener);

        context=this;


        Bundle extras=getIntent().getExtras();

        if(extras == null) {
            UserID= -1;
        }
        else {
            UserID = extras.getLong("UserID");
            if(UserID >0)
                getUserById();
        }


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)
                .setFastestInterval(1 * 1000);

        toPointsNames = new ArrayList<>();

        spinner.setOnItemSelectedListener(this);
        Call<List<Point>> call = pointService.getPoints();
        call.enqueue(new CustomCallback<List<Point>>(this) {

            @Override
            public void onSuccess(List<Point> model) {
                toPoints = new ArrayList<Point>(model);
                getNamesOfPoints();

            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("error", t.toString());
            }

            @Override
            public void onResponse(Response<List<Point>> response, Retrofit retrofit) {
                toPoints = new ArrayList<Point>(response.body());
                getNamesOfPoints();

            }
        });
    }



    private void getNamesOfPoints(){
        toPointsNames = new ArrayList<>();
        Random rand = new Random();
        int fromID = rand.nextInt(toPoints.size()) + 1;
        if(toPoints!= null){
            for (Point p : toPoints ) {
                toPointsNames.add(p.Name);
                if(p.ID == fromID)
                    locationPoint = new Point(p);
            }

            final ArrayAdapter<String> adapter = new ArrayAdapter<String>(MapActivity.this,
                    android.R.layout.simple_spinner_item,toPointsNames);

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
        }else{
            Toast.makeText(this, "toPoints is null!", Toast.LENGTH_SHORT).show();
        }

    }

    public void onItemSelected(AdapterView<?> parent, View v, int position, long id)
    {
        String selected=spinner.getSelectedItem().toString();
        Log.d("Wybrano", spinner.getSelectedItem().toString());
        for(Point p:toPoints)
        {
            if(p.Name==selected)
                newFare.setDestinations(Arrays.asList(p));
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }




    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.moveCamera(CameraUpdateFactory.newLatLng(PALACEOFCULTURE));

    }



    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
    location= LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
        else {
            handleNewLocation(location);
        }
    }

    private void handleNewLocation(Location location)
    {
        Log.d(TAG, location.toString());


        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        LatLng latLng = new LatLng(currentLatitude, currentLongitude);

        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .title("I am here!");
        mMap.addMarker(options).showInfoWindow();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
        mMap.animateCamera(cameraUpdate);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Location services suspended. Please reconnect.");
        Toast.makeText(getApplicationContext(), "Location services suspended. Please reconnect.",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.d(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
            Toast.makeText(getApplicationContext(), "Location services connection failed with code " + connectionResult.getErrorCode(),
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }


    public void ChangeView(View view) {
        checkPopUp();
    }
    private void checkPopUp() {
        try {
            //We need to get the instance of the LayoutInflater, use the context of this activity
            LayoutInflater inflater = (LayoutInflater) getBaseContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //Inflate the view from a predefined XML layout
            View layout = inflater.inflate(R.layout.now_popup_layout, null, false);


            // create a 300px width and 470px height PopupWindow
            pw = new PopupWindow(layout, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT, true);
            // display the popup in the center
            pw.showAtLocation(layout, Gravity.CENTER, 0, 0);

            pw.setOutsideTouchable(true);
            pw.setFocusable(true);
            pw.update();


            Button cancelButton = (Button) layout.findViewById(R.id.cancel_button);
            Button submitButton = (Button) layout.findViewById(R.id.submit_button);


            cancelButton.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    pw.dismiss();
                }
            });

            submitButton.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {

                    LinearLayout linearLayout = (LinearLayout)getLayoutInflater().inflate(R.layout.now_popup_layout, null);

                    final String name = ((EditText)linearLayout.findViewById(R.id.name_popup)).getText().toString();
                    final String surname =((EditText)linearLayout.findViewById(R.id.surname_popup)).getText().toString();
                    final String telephone = ((EditText)linearLayout.findViewById(R.id.telephone_popup)).getText().toString();

                    final ProgressDialog progressDialog = new ProgressDialog(MapActivity.this,
                            R.style.AppTheme_Dark_Dialog);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage("Looking for a taxi...");
                    progressDialog.show();

                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    progressDialog.dismiss();
                                    FareNew model=new FareNew();

                                    double currentLatitude = location.getLatitude();
                                    double currentLongitude = location.getLongitude();
                                    model.fromX=currentLatitude;
                                    model.fromY=currentLongitude;
                                    model.when= new Date();
                                    if(toLocation!=null) {
                                        model.toX = toLocation.latitude;
                                        model.toY = toLocation.longitude;
                                    }
                                    sendSMSMessage(ekspres,"Taxi", currentLatitude, currentLongitude);



                                    pw.dismiss();
                                }
                            },3000);

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void sendSMSMessage(String Name, String Surname, double X, double Y) {
        Log.i("Send SMS", "");
        String phoneNo = "883851967";
        String message;
        if(Name.equals(ekspres)){
            message = Name + " " + Surname + " is coming to: coordinates: " + X + " " + Y;
        }else {
            message = "A customer: " + Name + " " + Surname + " ordered a taxi. Coordinates: " + X + " " + Y;
        }
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, message, null, null);
            Toast.makeText(getApplicationContext(), "SMS sent to a taxi.", Toast.LENGTH_LONG).show();
        }

        catch (Exception e) {
            Toast.makeText(getApplicationContext(), "SMS faild, please try again.", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public boolean onTouchEvent(MotionEvent touchevent) {

        switch (touchevent.getAction()) {

            case MotionEvent.ACTION_DOWN:
                lastX = touchevent.getX();

                break;

            case MotionEvent.ACTION_UP:

                float currentX = touchevent.getX();

                if(lastX<currentX)
                {
                    if(viewFlipper.getDisplayedChild()==0)
                        break;
                    viewFlipper.setInAnimation(this, R.anim.slide_in_from_left);

                    viewFlipper.setOutAnimation(this, R.anim.slide_out_to_right);

                    viewFlipper.showPrevious();

                }
                break;

        }
        return false;

    }


    private View.OnClickListener newCourseBtnListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {


            if (viewFlipper.getDisplayedChild() == 2)
                return;
            viewFlipper.setInAnimation(context, R.anim.slide_in_from_right);
            viewFlipper.setOutAnimation(context, R.anim.slide_out_to_left);
            viewFlipper.showNext();

            new android.os.Handler().postDelayed(new Runnable() {
                @Override
                public void run() {


                    if (viewFlipper.getDisplayedChild() == 2) {

                        if (UserID <= 0) {
                            Intent fbIntent = new Intent(MapActivity.this, FacebookActivity.class);
                            startActivity(fbIntent);
                        } else {
                            progressDialog = new ProgressDialog(MapActivity.this,
                                    R.style.AppTheme_Dark_Dialog);
                            progressDialog.setIndeterminate(true);
                            progressDialog.setMessage("Waiting for people to share taxi...");
                            progressDialog.show();

                            new android.os.Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialog.dismiss();
                                    final Intent intent = new Intent(MapActivity.this, FoundCoursesActivity.class);
                                    intent.putExtra("TO", newFare.getDestinations().get(0).Name);
                                    intent.putExtra("UserID", UserID);
                                    MapActivity.this.startActivity(intent);
                                    MapActivity.this.finish();
                                }
                            }, 3000);


                        }


                    }
                }
            }, 500);

        }
    };

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.privateRadio:
                if (checked)
                    newFare.setType(DataProvider.FareType.single);
                break;
            case R.id.womenRadio:
                if (checked)
                    newFare.setType(DataProvider.FareType.women);
                break;
            case R.id.everyoneRadio:
                if (checked)
                    newFare.setType(DataProvider.FareType.everyone);
                break;
        }
        Log.d("TYPE SELECTED:",newFare.getType().toString());
    }

    public LatLng getLocationFromAddress(String strAddress){

        Geocoder coder = new Geocoder(this);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress,5);
            if (address==null) {
                return null;
            }
            Address location=address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude() );


        }
        catch (IOException e)
        {
            Log.d("BAD ADRESS",strAddress);
        }
        return p1;
    };


    private  TextView.OnEditorActionListener destinationEditListener=new TextView.OnEditorActionListener()
{       @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {

                LatLng latLng=getLocationFromAddress(v.getText().toString());
                if(latLng!=null) {
                    MarkerOptions options = new MarkerOptions()
                            .position(latLng)
                            .title("Destination")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                    toLocation=latLng;
                    mMap.addMarker(options).showInfoWindow();
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
                    mMap.animateCamera(cameraUpdate);


                }
            }
            return false;
        }
    };





    private View.OnClickListener bookBtnListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            progressDialog = new ProgressDialog(MapActivity.this,
                    R.style.AppTheme_Dark_Dialog);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Preparing your taxi...");
            progressDialog.show();

            FareNew model = setNewFare();

            if (UserID > 0) {
                Call<ResponseBody> call = fareService.postFare(model);
                call.enqueue(new CustomCallback<ResponseBody>(MapActivity.this) {
                    @Override
                    public void onSuccess(ResponseBody model) {
                        Toast.makeText(MapActivity.this, "Your fare created", Toast.LENGTH_LONG).show();
                        double currentLatitude = location.getLatitude();
                        double currentLongitude = location.getLongitude();
                        if(userDetails!=null) {
                            sendSMSMessage(userDetails.Name, userDetails.Surname, currentLatitude,currentLongitude);
                        }

                        new android.os.Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                progressDialog.dismiss();
                                final Intent intent = new Intent(MapActivity.this, WelcomeActivity.class);
                                intent.putExtra("UserID", UserID);
                                MapActivity.this.startActivity(intent);
                                MapActivity.this.finish();
                            }
                        }, 3000);

                    }

                    @Override
                    public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                        if (response.code() == 400) {
                            //handleBadRequest(response);
                            Toast.makeText(MapActivity.this, "wrong request: PostFare", Toast.LENGTH_LONG).show();
                        } else {
                            super.onResponse(response, retrofit);
                        }
                    }

                });

            }
            else
            {
                Intent fbIntent=new Intent(MapActivity.this,FacebookActivity.class);
                startActivity(fbIntent);
            }
        }

    };

    private void getUserById(){
        Call<UserDetails> call = userService.getUser(UserID);
        call.enqueue(new CustomCallback<UserDetails>(this) {

            @Override
            public void onSuccess(UserDetails model) {
                userDetails = new UserDetails(model);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("error", t.toString());
            }

            @Override
            public void onResponse(Response<UserDetails> response, Retrofit retrofit) {
                userDetails = new UserDetails(response.body());
            }
        });
    }

    private FareNew setNewFare(){
        FareNew newFareNew = new FareNew();
        newFareNew.FareID = newFare.getId();
        newFareNew.fromX = locationPoint.CoordX;
        newFareNew.fromY = locationPoint.CoordY;
        newFareNew.UserID = UserID;
        newFareNew.when = newFare.getDate();
        newFareNew.toX = newFare.getDestinations().get(0).CoordX;
        newFareNew.toY = newFare.getDestinations().get(0).CoordY;
        return newFareNew;

    }


}

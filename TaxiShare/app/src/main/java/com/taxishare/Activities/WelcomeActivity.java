package com.taxishare.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.taxishare.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class WelcomeActivity extends AppCompatActivity {

    private Context context;
    private long UserID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        Button loginBtn=(Button) findViewById(R.id.btn_login);
        Button registerBtn=(Button) findViewById(R.id.btn_register);
        Button unregistereBtn=(Button) findViewById(R.id.unregisterd);
        loginBtn.setOnClickListener(loginBtnListener);
        registerBtn.setOnClickListener(registerBtnListener);
        unregistereBtn.setOnClickListener(unregisterBtnListener);
        context=this;

        Bundle extras=getIntent().getExtras();

        if(extras == null) {

            UserID=-1;
        }
        else {

            UserID=extras.getLong("UserID");
        }


        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.facebook.samples.hellofacebook",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

    }
   private View.OnClickListener loginBtnListener=new View.OnClickListener() {
       @Override
        public void onClick(View v) {
         Intent intent=new Intent(WelcomeActivity.this,FacebookActivity.class);
           startActivity(intent);

        }
    };
    private View.OnClickListener registerBtnListener=new View.OnClickListener() {
        @Override
       public void onClick(View v) {


            Intent intent=new Intent(WelcomeActivity.this,RegisterActivity.class);

            startActivity(intent);
        }
    };
    private View.OnClickListener unregisterBtnListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {


            Intent intent=new Intent(WelcomeActivity.this,MapActivity.class);
            intent.putExtra("UserID", UserID);
            startActivity(intent);
        }
    };
}

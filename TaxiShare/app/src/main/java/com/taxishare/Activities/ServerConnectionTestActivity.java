package com.taxishare.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.taxishare.Infrastructure.CustomCallback;
import com.taxishare.MyApplication;
import com.taxishare.R;
import com.taxishare.Services.ServerConnectionTestService;

import javax.inject.Inject;

import retrofit.Call;

public class ServerConnectionTestActivity extends AppCompatActivity {

    @Inject
    ServerConnectionTestService service;

    TextView txtView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_connection_test);

        ((MyApplication) getApplication()).getServicesComponent().inject(this);


        txtView = (TextView) findViewById(R.id.testTextView);

        Call<Boolean> call = service.isParticipant("aa", 1);
        call.enqueue(new CustomCallback<Boolean>(this)
        {
            @Override
            public void onSuccess(Boolean model)
            {
                txtView.setText(model.toString());
            }
        });


    }
}

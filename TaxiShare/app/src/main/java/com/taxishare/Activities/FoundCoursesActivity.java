package com.taxishare.Activities;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ListView;
import android.widget.Toast;

import com.squareup.okhttp.ResponseBody;
import com.taxishare.Delegate;
import com.taxishare.Infrastructure.CustomCallback;
import com.taxishare.Models.FareDTO;
import com.taxishare.Models.FareNew;
import com.taxishare.Models.Point;
import com.taxishare.MyApplication;
import com.taxishare.R;
import com.taxishare.Services.FareService;
import com.taxishare.Services.PointService;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;

public class FoundCoursesActivity extends AppCompatActivity {
    public  static  final String CHOOSENFARE="com.taxishare.choosenfare";
    ProgressDialog progressDialog=null;
    String toString;
    private  float lastX;
    private List<Point> allPoints;
    private List<FareDTO> fareList=null;
    private ListView listView;
    private long UserID;

    @Inject    FareService fareService;
    @Inject    PointService pointService;

    @Inject    SharedPreferences preferences;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_found_courses2);

        ((MyApplication) getApplication()).getServicesComponent().inject(this);


        Initialize();
        Bundle extras=getIntent().getExtras();

        if(extras == null) {
            toString= "";
            UserID=-1;
        }
        else {
            toString = extras.getString("TO");
            UserID=extras.getLong("UserID");
        }


        listView=(ListView) findViewById(R.id.listView2);

        Call<List<Point>> call = pointService.getPoints();
        call.enqueue(new CustomCallback<List<Point>>(this) {

            @Override
            public void onSuccess(List<Point> model) {
                allPoints = new ArrayList<Point>(model);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("error", t.toString());
            }

            @Override
            public void onResponse(Response<List<Point>> response, Retrofit retrofit) {
                allPoints = new ArrayList<Point>(response.body());
            }
        });

    }

    private Context context = this;

    private void Initialize()
    {

        Call<List<FareDTO>> call = fareService.getFares();
        call.enqueue(new CustomCallback<List<FareDTO>>(this) {

            @Override
            public void onSuccess(List<FareDTO> model) {
                fareList = new ArrayList<FareDTO>(model);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("error", t.toString());
            }

            @Override
            public void onResponse(Response<List<FareDTO>> response, Retrofit retrofit) {
                fareList = new ArrayList<FareDTO>(response.body());
                for (FareDTO fare: fareList) {
                }
                com.taxishare.Adapters.MyAdapter adapter = new com.taxishare.Adapters.MyAdapter(fareList,toString, new DelegateImpl(), context);
                listView.setAdapter(adapter);
            }
        });
    }

    private void joinFare(FareNew fareNew, String toPointName){
        fareNew.UserID = UserID;
        for (Point p: allPoints) {
            if(toPointName.equals(p.Name)){
                fareNew.toX = p.CoordX;
                fareNew.toY = p.CoordY;
                break;
            }
        }

        Call<ResponseBody> call = fareService.postJoinFare(fareNew);
        call.enqueue(new CustomCallback<ResponseBody>(this) {
            @Override
            public void onSuccess(ResponseBody model) {
                Toast.makeText(MyApplication.getContext(), "You joined this fare", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                Toast.makeText(MyApplication.getContext(), "You joined this fare", Toast.LENGTH_LONG).show();
                if (response.code() == 400) {
                    Toast.makeText(MyApplication.getContext(), "wrong request: PostFare", Toast.LENGTH_LONG).show();
                } else {
                    super.onResponse(response, retrofit);
                }
            }


        });

    }



    public boolean onTouchEvent(MotionEvent touchevent) {

        switch (touchevent.getAction()) {

            case MotionEvent.ACTION_DOWN:
                lastX = touchevent.getX();

                break;

            case MotionEvent.ACTION_UP:

                float currentX = touchevent.getX();

                if(lastX<currentX)
                {
                    Intent intent=new Intent(FoundCoursesActivity.this,MapActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    Bundle bndlanimation =
                            ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_in_from_left, R.anim.slide_out_to_right).toBundle();
                    startActivity(intent, bndlanimation);


                }
                break;

        }
        return false;

    }
    class DelegateImpl implements Delegate {
        public void Execute(FareNew fareNew, String toPointName){
            joinFare(fareNew, toPointName);
        }
    }



}

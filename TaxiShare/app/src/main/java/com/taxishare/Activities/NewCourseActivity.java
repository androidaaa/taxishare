package com.taxishare.Activities;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.taxishare.DataProvider;
import com.taxishare.Models.Fare;
import com.taxishare.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NewCourseActivity extends AppCompatActivity {

   static Fare newFare=null;
    static EditText DateEdit=null;
    static EditText TimeEdit=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_course);
        newFare=new Fare();


      InitializeUI();

    }
    public View.OnClickListener addBtnListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {


            Log.d("NEW COURSE",newFare.toString());
            Intent intent=new Intent(NewCourseActivity.this,LoginActivity.class);
            startActivity(intent);
        }
    };

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(this.getFragmentManager(), "timePicker");

    }
    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

        public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()) {
            case R.id.privateRadio:
                if (checked)
                    newFare.setType(DataProvider.FareType.single);
                    break;
            case R.id.womenRadio:
                if (checked)
                    newFare.setType(DataProvider.FareType.women);
                    break;
            case R.id.everyoneRadio:
                if (checked)
                    newFare.setType(DataProvider.FareType.everyone);
                    break;
        }
        Log.d("TYPE SELECTED:",newFare.getType().toString());
    }


    private void InitializeUI()
    {

        Button addBtn=(Button) findViewById(R.id.addCourseBtn);
        addBtn.setOnClickListener(addBtnListener);
        DateEdit=(EditText)findViewById(R.id.newCourseDate);
        TimeEdit=(EditText)findViewById(R.id.newCourseTime);


        Spinner fromSpinner=(Spinner)findViewById(R.id.from_spinner);
        List<String> places= Arrays.asList("Centrum", "GUS", "Tesco Kabaty", "Metro Politechnika");
        ArrayAdapter<String> adapter_state = new ArrayAdapter<String>(this,  android.R.layout.simple_spinner_item, places);
        fromSpinner.setAdapter(adapter_state);
        fromSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });


        Spinner toSpinner=(Spinner)findViewById(R.id.to_spinner);
        ArrayAdapter<String> adapter_to = new ArrayAdapter<String>(this,  android.R.layout.simple_spinner_item, places);
        toSpinner.setAdapter(adapter_to);
        toSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                List<String> dest=new ArrayList<String>();
                dest.add(parentView.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });


    }


    public static class DatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            DateEdit.setText(day + "/" + (month + 1) + "/" + year);
            Date date=new Date(year,month+1,day);
            newFare.setDate(date);
        }
    }


    public static class TimePickerFragment extends DialogFragment implements
            TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            TimeEdit.setText(hourOfDay + ":" + minute);
            Date date=newFare.getDate();
            date.setMinutes(minute);
            date.setHours(hourOfDay);
            newFare.setDate(date);
        }
    }

    }



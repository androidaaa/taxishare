package com.taxishare.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.taxishare.R;

public class LocationActivity extends AppCompatActivity {

    private Spinner spinner_start, spinner_end;
    private TextView startPlace, endPlace;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        startPlace = (TextView) findViewById(R.id.StartPlace);
        endPlace = (TextView) findViewById(R.id.EndPlace);

        spinner_start = (Spinner) findViewById(R.id.Start);
        spinner_end = (Spinner) findViewById(R.id.End);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.start_location_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_start.setAdapter(adapter);
        spinner_end.setAdapter(adapter);

    }

    public void ShowLocation(View view) {
         Intent intent = new Intent(this, MapActivity.class);
         startActivity(intent);
    }

    public void SavePlace(View view) {
        if (spinner_start.getSelectedItem().toString()!= spinner_end.getSelectedItem().toString()) {
            Toast.makeText(getApplicationContext(), "Journey location saved",
                    Toast.LENGTH_SHORT).show();
             Intent i = new Intent(this, DateTimeActivity.class);
             startActivity(i);
        }
        else
            Toast.makeText(getApplicationContext(), "Choose different start/destination location",
                    Toast.LENGTH_SHORT).show();
    }
}

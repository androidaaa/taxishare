package com.taxishare.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.taxishare.Infrastructure.CustomCallback;
import com.taxishare.Models.AccessTokenModel;
import com.taxishare.MyApplication;
import com.taxishare.R;
import com.taxishare.Services.AccountService;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;
    private static final String GRANT_TYPE = "password";
    public static final String ACCESS_TOKEN_KEY = "accessToken";
    public static final String USERNAME_KEY = "username";

    private static final String login_app = "login";
    private static final String password_app = "password";

    private ProgressDialog progressDialog;

    @Inject AccountService service;
    @Inject SharedPreferences preferences;

    @Bind(R.id.input_login) EditText _loginText;
    @Bind(R.id.input_password) EditText _passwordText;
    @Bind(R.id.btn_login) Button _loginButton;
    @Bind(R.id.link_signup) TextView _signupLink;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        ((MyApplication) getApplication()).getServicesComponent().inject(this);
        Intent intent=getIntent();


        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });

        _signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO: Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
            }
        });
    }


    public void login() {
        Log.d(TAG, "Login");

        String login = _loginText.getText().toString();
        String password = _passwordText.getText().toString();

        if (!validate(login, password)) {
            onLoginFailed();
            return;
        }


        progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();





        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        progressDialog.dismiss();
                        onLoginSuccess();
                    }
                }, 3000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {

                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically

                this.finish();
            }
        }
    }

    public void onLoginSuccess() {
        _loginButton.setEnabled(true);
        Intent intent=new Intent(LoginActivity.this,JoinCourseActivity.class);
        startActivity(intent);
        finish();
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _loginButton.setEnabled(true);
    }


    public boolean validate(String login, String password) {
        boolean valid = true;

        if (login.isEmpty()) {
            _loginText.setError("enter login");
            valid = false;
        }
        else {
            _loginText.setError(null);
            if (password.isEmpty()) {
                _passwordText.setError("password empty");
                valid = false;
            } else {
                _passwordText.setError(null);
            }
        }

        return valid;
    }

    private void getToken(final String username, String password) {
        Call<AccessTokenModel> call = service.getToken(username, password, GRANT_TYPE);
        call.enqueue(new CustomCallback<AccessTokenModel>(this) {
            @Override
            public void onSuccess(AccessTokenModel model) {
                String token = model.getAccessToken();
                if(!TextUtils.isEmpty(token)) {
                    SharedPreferences.Editor edit = preferences.edit();
                    edit.putString(ACCESS_TOKEN_KEY, token);
                    edit.putString(USERNAME_KEY, username);
                    edit.apply();
                    onLoginSuccess();
                    //showMainActivity();
                }
            }

            @Override
            public void onResponse(Response<AccessTokenModel> response, Retrofit retrofit) {
                if(!response.isSuccess() && response.code() == 400) {
                    progressDialog.dismiss();
                    showIncorrectCredentialsDialog();
                } else {
                    super.onResponse(response, retrofit);
                }
            }


            @Override
            public void always() {
                progressDialog.dismiss();
            }
        });
    }

    private void showIncorrectCredentialsDialog() {
        new AlertDialog.Builder(LoginActivity.this)
                .setTitle(R.string.incorrect_credentials)
                .setMessage(R.string.incorrect_credentials_message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}


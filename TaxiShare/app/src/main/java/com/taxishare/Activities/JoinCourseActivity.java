package com.taxishare.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.taxishare.Adapters.ExpandableListAdapter;
import com.taxishare.Infrastructure.CustomCallback;
import com.taxishare.Models.FareDTO;
import com.taxishare.Models.FareDetailsDTO;
import com.taxishare.Models.UserDTO;
import com.taxishare.MyApplication;
import com.taxishare.R;
import com.taxishare.Services.FareService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;

public class JoinCourseActivity extends AppCompatActivity {

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    FareDetailsDTO fare;
    long ID;
    TextView detailTaxiName;
    TextView detailTelephone;
    TextView detailPlace;
    TextView detailTime;
    TextView deatailDate;
    @Inject FareService service;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_course);
        ((MyApplication) getApplication()).getServicesComponent().inject(this);

       final Context context = this;
        Bundle extras=getIntent().getExtras();
        if(extras == null) {
            ID=0;
        }
        else {
            ID = extras.getLong("CHOSENFAREID");
        }

        // get the listview
        expListView = (ExpandableListView) findViewById(R.id.peopleLV);

        Initialize();

        Call<FareDetailsDTO> call = service.getFare(ID);
        call.enqueue(new CustomCallback<FareDetailsDTO>(this) {

            @Override
            public void onSuccess(FareDetailsDTO model) {
                fare = model;
                prepareTextViews(fare);
                prepareListData(fare);

                listAdapter = new ExpandableListAdapter(context, listDataHeader, listDataChild);

                // setting list adapter
                expListView.setAdapter(listAdapter);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("error", t.toString());
            }

            @Override
            public void onResponse(Response<FareDetailsDTO> response, Retrofit retrofit) {
                fare = (response.body());
                // preparing list data
                prepareTextViews(fare);
                prepareListData(fare);

                listAdapter = new ExpandableListAdapter(context, listDataHeader, listDataChild);

                // setting list adapter
                expListView.setAdapter(listAdapter);
            }
        });


        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void prepareListData(FareDetailsDTO fare) {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("Pals to share");

        // Adding child data
        List<String> people = new ArrayList<String>();
        for(UserDTO u:fare.UserList)
        {
            people.add(u.Login);
        }

        listDataChild.put(listDataHeader.get(0), people); // Header, Child data
    }
    private void prepareTextViews(FareDetailsDTO fare)
    {
        detailTelephone.setText(fare.Taxi.PhoneNumber);
        detailTaxiName.setText(fare.Taxi.DriverName+" "+fare.Taxi.DriverSurname);
        detailPlace.setText(fare.From);
        String dateTime[]=fare.When.split("T");
        if(dateTime.length==2) {
            deatailDate.setText(dateTime[0]);
            detailTime.setText(dateTime[1]);
        }
        else
        {
            deatailDate.setText("2016-06-11");
            detailTime.setText("8:30");
        }


    }
    private void Initialize()
    {
        detailTaxiName= (TextView)findViewById(R.id.detail_taxiName);
        detailTelephone=(TextView)findViewById(R.id.detail_telephone);
        detailPlace=(TextView)findViewById(R.id.detail_place);
        deatailDate=(TextView)findViewById(R.id.detail_date);
        detailTime=(TextView)findViewById(R.id.detail_time);
    }
}

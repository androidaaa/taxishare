package com.taxishare.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.squareup.okhttp.ResponseBody;
import com.taxishare.Infrastructure.CustomCallback;
import com.taxishare.Models.Gender;
import com.taxishare.Models.RegisterAccountModel;
import com.taxishare.MyApplication;
import com.taxishare.R;
import com.taxishare.Services.AccountService;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;

public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = "SignupActivity";
    private ProgressDialog progressDialog;

    @Bind(R.id.input_email) EditText _emailText;
    @Bind((R.id.input_name)) EditText _nameText;
    @Bind(R.id.input_surname) EditText _surnameText;
    @Bind(R.id.input_password) EditText _passwordText;
    @Bind(R.id.input_confirm_password) EditText _confirmPasswordText;
    @Bind(R.id.btn_signup) Button _signupButton;
    @Bind(R.id.link_login) TextView _loginLink;
    @Bind(R.id.sexRadio) RadioGroup _sexRadioGroup;
    @Bind(R.id.sexFemaleRadio) RadioButton _femaleRadio;
    @Bind(R.id.sexMaleRadio) RadioButton _maleRadio;

    @Inject
    AccountService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ((MyApplication) getApplication()).getServicesComponent().inject(this);
        ButterKnife.bind(this);

        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                finish();
            }
        });
    }

    public void signup() {
        Log.d(TAG, "Signup");

        if (!validate()) {
            onSignupFailed();
            return;
        }

        _signupButton.setEnabled(false);

        progressDialog = new ProgressDialog(RegisterActivity.this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");
        progressDialog.show();

        String email = _emailText.getText().toString();
        String name = _nameText.getText().toString();
        String surname = _surnameText.getText().toString();
        String password = _passwordText.getText().toString();
        Gender gender = _femaleRadio.isChecked() ? Gender.Female : Gender.Male;
        RegisterAccountModel model = new RegisterAccountModel(email, name, surname, password, gender);

        Call<ResponseBody> call = service.registerAccount(model);
        call.enqueue(new CustomCallback<ResponseBody>(RegisterActivity.this) {
            @Override
            public void onSuccess(ResponseBody model) {
                Toast.makeText(RegisterActivity.this, R.string.success_account_registered, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(RegisterActivity.this, MapActivity.class);
                startActivity(intent);
            }

            @Override
            public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                if (response.code() == 400) {
                    handleBadRequest(response);
                } else {
                    super.onResponse(response, retrofit);
                }
            }

            @Override
            public void always() {
                progressDialog.dismiss();
            }
        });

        // TODO: Implement your own signup logic here.
        /*
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onSignupSuccess or onSignupFailed
                        // depending on success
                        //onSignupSuccess();
                        // onSignupFailed();
                        progressDialog.dismiss();
                        Toast.makeText(getBaseContext(), "Registration successful", Toast.LENGTH_LONG).show();
                        finish();
                    }
                }, 3000);*/
    }

    private void handleBadRequest(Response<ResponseBody> response) {
        progressDialog.dismiss();
        try {
            new AlertDialog.Builder(RegisterActivity.this)
                    .setTitle(getString(R.string.error))
                    .setMessage(extractErrorMessage(response))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } catch (IOException e) {
            Log.e("Register", e.getMessage());
        }
    }
    private String extractErrorMessage(Response<ResponseBody> response) throws IOException {
        String json = response.errorBody().string();
        Gson gson = new Gson();
        ErrorResponse errorResponse = gson.fromJson(json, ErrorResponse.class);
        String errorMessage = TextUtils.join(" ", errorResponse.getErrorsMessages());
        return errorMessage;
    }


    public void onSignupSuccess() {
        _signupButton.setEnabled(true);
        setResult(RESULT_OK, null);
        finish();
    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Registration failed", Toast.LENGTH_LONG).show();

        _signupButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String name = _nameText.getText().toString();
        String surname = _surnameText.getText().toString();
        String password = _passwordText.getText().toString();
        String confirmPassword = _confirmPasswordText.getText().toString();
        int selected = _sexRadioGroup.getCheckedRadioButtonId();

        if (email.isEmpty() || email.length() < 3) {
            _emailText.setError("at least 3 characters");
            valid = false;

        }else{
            _emailText.setError(null);
        }
        if (name.isEmpty() || name.length() < 3) {
            _nameText.setError("at least 3 characters");
            valid = false;
        } else {
            _nameText.setError(null);
        }
        if (surname.isEmpty() || surname.length() < 3) {
            _surnameText.setError("at least 3 characters");
            valid = false;
        } else {
            _surnameText.setError(null);
        }
        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        }else if(!password.equals(confirmPassword)){
            _passwordText.setError("password don't much");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        if(confirmPassword.isEmpty()){
            _confirmPasswordText.setError("confirm password");
            valid = false;
        }else{
            _confirmPasswordText.setError(null);
        }

        if(selected==-1){
            _maleRadio.setError("choose your sex");
            valid = false;
        } else {
            _maleRadio.setError(null);
        }

        return valid;
    }

    public static boolean isValidEmail(CharSequence target) {
        Pattern pattern;
        Matcher matcher;

        final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(target);
        return matcher.matches();
    }

    public class ErrorResponse {
        private String message;
        private ModelState modelState;

        public String getMessage() {
            return message;
        }

        public String[] getErrorsMessages() {
            return modelState.getErrors();
        }

        private class ModelState
        {
            @SerializedName("")
            private String[] errors;

            public String[] getErrors() {
                return errors;
            }
        }
    }

}

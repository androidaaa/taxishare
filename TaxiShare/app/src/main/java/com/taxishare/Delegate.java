package com.taxishare;

import com.taxishare.Models.FareNew;


public interface Delegate {

    void Execute(FareNew fareNew, String toPointName);
}

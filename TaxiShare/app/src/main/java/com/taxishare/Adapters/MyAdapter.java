package com.taxishare.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.taxishare.Activities.JoinCourseActivity;
import com.taxishare.Delegate;
import com.taxishare.Models.FareDTO;
import com.taxishare.Models.FareNew;
import com.taxishare.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Agata on 2016-04-12.
 */
public class MyAdapter extends BaseAdapter implements ListAdapter {
    public  static  final String CHOOSENFARE="com.taxishare.choosenfare";
    private List<FareDTO> list = new ArrayList<FareDTO>();
    private Context context;
    private String toPointName;
    private final Delegate delegate;

    /*final Delegate delegate = new Delegate() {
        @Override
        public void Execute() {

        }
    };*/

    public MyAdapter(List<FareDTO> list,String to, Delegate delegate, Context context) {
        this.list = new ArrayList<FareDTO>(list);
        this.context = context;
        this.delegate = delegate;
        //((MyApplication) context.getApplicationContext()).getServicesComponent().inject(context.);
        /*delegate = new Delegate() {
            @Override
            public void Execute() {

            }
        };*/

        this.toPointName=to;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }


    public  Object getList()
    {
        return  list;
    }


    @Override
    public long getItemId(int pos) {
        // return list.get(pos).getId();
        return  0;
        //just return 0 if your list items do not have an Id variable.
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.found_courses_row, null);
        }

        //Handle TextView and display string from your list

       /* LinearLayout Placeslist = (LinearLayout) view.findViewById(R.id.places);
        Placeslist.removeAllViews();

        for (String place : list.get(position).getDestinations()) {
            View line = Placeslist.inflate(context,R.layout.support_simple_spinner_dropdown_item,null);

    /* nested list's stuff */

          //  Placeslist.addView(line);
        //}


        TextView textView=(TextView) view.findViewById(R.id.places);
        textView.setText(list.get(position).From);

        //Handle buttons and add onClickListeners
        TextView textView2 = (TextView) view.findViewById(R.id.passangersCount);
        textView2.setText(String.valueOf(list.get(position).UserListCount));
        Button joinBtn = (Button)view.findViewById(R.id.joinbtn);


        joinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                //do something
                // list.remove(position); //or some other task

                final FareDTO item = (FareDTO) getItem(position);
                FareNew fare = new FareNew();
                //fare.UserID = randId;
                fare.FareID = item.ID;
                fare.when = new Date();
                delegate.Execute(fare, toPointName);

                Log.d("You clicked at positon", item.From);
                Intent intent = new Intent(context, JoinCourseActivity.class);
                intent.putExtra("CHOSENFAREID", item.ID);
                context.startActivity(intent);
            }
        });

        return view;
    }
}
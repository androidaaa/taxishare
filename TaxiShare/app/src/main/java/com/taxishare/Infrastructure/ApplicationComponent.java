package com.taxishare.Infrastructure;

import com.taxishare.Activities.FacebookActivity;
import com.taxishare.Activities.FoundCoursesActivity;
import com.taxishare.Activities.JoinCourseActivity;
import com.taxishare.Activities.LoginActivity;
import com.taxishare.Activities.MapActivity;
import com.taxishare.Activities.RegisterActivity;
import com.taxishare.Activities.ServerConnectionTestActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {RestServicesModule.class, ApplicationModule.class})
public interface ApplicationComponent {
    //void inject(MainActivity activity);
    void inject(LoginActivity activity);
    void inject(ServerConnectionTestActivity activity);
    void inject(FoundCoursesActivity activity);
    void inject(RegisterActivity activity);
    void inject(MapActivity activity);
    void inject(FacebookActivity activity);
    void inject(JoinCourseActivity activity);



}

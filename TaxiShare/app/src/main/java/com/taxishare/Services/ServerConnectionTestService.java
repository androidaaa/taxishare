package com.taxishare.Services;

import com.squareup.okhttp.ResponseBody;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;


public interface ServerConnectionTestService {

    @POST("Players/Test/{a}")
    Call<ResponseBody> testNoAuth(@Path("a") int a);

    @GET("players/isParticipant")
    Call<Boolean> isParticipant(@Query("username") String username, @Query("meetingId") int meetingId);
}

package com.taxishare.Services;

import com.taxishare.Models.User;
import com.taxishare.Models.UserDetails;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;


public interface UserService {
    @GET("api/User/AllUsers")
    Call<List<User>> get();

    @GET("api/User/GetOne/{id}")
    Call<UserDetails> getUser(@Query("id") long id);

    @GET("api/User/GetId")
    Call<Long> getId(@Query("login")String login);

}

package com.taxishare.Services;

import com.squareup.okhttp.ResponseBody;
import com.taxishare.Models.FareDTO;
import com.taxishare.Models.FareDetailsDTO;
import com.taxishare.Models.FareNew;

import java.util.List;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;

import retrofit.http.Query;


public interface FareService {


    @GET("/api/Fare/GetOne/{id}")
    Call<FareDetailsDTO> getFare(@Query("id") long id);


    @GET("/api/Fare/AllFares")
    Call<List<FareDTO>> getFares();

    @POST("/api/Fare/PostFare")
    Call<ResponseBody> postFare(@Body FareNew fareNewModel);

    @POST("/api/Fare/Join")
    Call<ResponseBody> postJoinFare(@Body FareNew fareNewModel);

}

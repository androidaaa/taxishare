package com.taxishare.Services;

import com.squareup.okhttp.ResponseBody;
import com.taxishare.Models.AccessTokenModel;
import com.taxishare.Models.RegisterAccountModel;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;


public interface AccountService {
    @POST("token")
    @FormUrlEncoded
    Call<AccessTokenModel> getToken(@Field("username") String username,
                                    @Field("password") String password,
                                    @Field("grant_type") String grantType);

    @POST("api/User/PostUser")
    Call<ResponseBody> registerAccount(@Body RegisterAccountModel registerAccountModel);
}
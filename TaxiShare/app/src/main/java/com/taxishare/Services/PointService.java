package com.taxishare.Services;

import com.taxishare.Models.Point;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;


public interface PointService {
    @GET("/api/Point")
    Call<List<Point>> getPoints();
}

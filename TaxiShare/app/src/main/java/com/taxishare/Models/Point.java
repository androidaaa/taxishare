package com.taxishare.Models;


public class Point {

    public long ID;
    public String Name;
    public double CoordX;
    public double CoordY;
    public int Votes;
    public boolean IsDefault;

    public Point(Point p){
        ID = p.ID;
        Name = p.Name;
        CoordX = p.CoordX;
        CoordY = p.CoordY;
        Votes = p.Votes;
        IsDefault = p.IsDefault;
    }
}

package com.taxishare.Models;

import com.google.gson.annotations.SerializedName;
import com.taxishare.DateTime;

import java.util.List;

/**
 * Created by Agata on 2016-06-10.
 */
public class FareDetailsDTO {
    @SerializedName("From")
    public String From;
    @SerializedName("To")
    public String To;
    @SerializedName("When")
    public String When;
    @SerializedName("Cost")
    public double Cost ;
    @SerializedName("UserList")
    public List<UserDTO> UserList;
    @SerializedName("Taxi")
    public Taxi Taxi;
    @SerializedName("ID")
    public long ID;
}

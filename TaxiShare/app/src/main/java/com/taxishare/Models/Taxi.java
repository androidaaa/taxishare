package com.taxishare.Models;

import com.google.gson.annotations.SerializedName;


public class Taxi {
    @SerializedName("ID")
    public long ID;
    @SerializedName("DriverName")
    public String DriverName;
    @SerializedName("DriverSurname")
    public String DriverSurname;
    @SerializedName("PhoneNumber")
    public String PhoneNumber;
}

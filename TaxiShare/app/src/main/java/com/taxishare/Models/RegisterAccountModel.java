package com.taxishare.Models;


public class RegisterAccountModel {
    private String login;
    private String name;
    private  String surname;
    private String password;
    private boolean gender;

    public RegisterAccountModel(String email, String name, String surname, String password, Gender gender) {
        this.login = email;
        this.name = name;
        this.surname = surname;
        this.password = password;
        if(gender==Gender.Female)
            this.gender = true;
        else
            this.gender = false;
    }
}

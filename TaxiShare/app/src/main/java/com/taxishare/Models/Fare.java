package com.taxishare.Models;

import com.taxishare.DataProvider;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Fare {

    private  int Id;
    private Point From;
   private List<Point> Destinations;
   private int passangerCount;
    private DataProvider.FareType type;
    private Date date;

    public  int getId()
    {
        return  Id;
    }
    public  void setId(int value)
    {
        Id=value;
    }
    public  Point getFrom()
    {return From;}
    public void setFrom(Point value)
    {
     From=value;
    }
    public  List<Point> getDestinations()
    {
        return  Destinations;
    }
    public  void setDestinations(List<Point> value)
    {
        Destinations=new ArrayList<Point>(value);
    }
    public int getPassangerCount()
    {
        return  passangerCount;
    }
    public  void setPassangerCount (int value)
    {
        passangerCount=value;
    }

    public  DataProvider.FareType getType()
    {
        return  type;
    }

    public  Date getDate()
    {
        return  date;
    }
    public  void  setDate(Date value)
    {
        date=value;
    }
    public  void setType(DataProvider.FareType value)
    {
        type=value;
    }

    public  Fare (Point from, List<Point> destinations)
    {
        From=from;
        Destinations=new ArrayList<Point>(destinations);
        passangerCount=Destinations.size();
        date= new Date();
    }
    public  Fare()
    {
        Destinations=new ArrayList<Point>();
        date= new Date();
    }

    @Override
    public String toString() {
        return "From:"+getFrom()+" To:"+getDestinations()+"Type:"+getType()+"Time:"+getDate();
    }
}

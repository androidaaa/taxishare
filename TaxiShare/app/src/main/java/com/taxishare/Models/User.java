package com.taxishare.Models;


public class User {

    private String username;
    private String password;
    private String confirm_password;
    private Gender sex;

    public User(){

    }
    public  User(String _username, String _password, String _confirm_password, Gender _sex){
        this.username = _username;
        this.password = _password;
        this.confirm_password = _confirm_password;
        this.sex = _sex;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Gender getSex() {
        return sex;
    }

    public void setSex(Gender sex) {
        this.sex = sex;
    }
}


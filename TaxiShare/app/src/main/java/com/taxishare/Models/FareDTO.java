package com.taxishare.Models;

import com.google.gson.annotations.SerializedName;


public class FareDTO {

    @SerializedName("ID")
    public long ID;
    @SerializedName("From")
    public String From;
    @SerializedName("To")
    public String To;
    @SerializedName("When")
    public String When;
    @SerializedName("UserListCount")
    public int UserListCount;

/*    private List<String> Destinations;
    private int passangerCount;
    private DataProvider.FareType type;
    private Date date;*/

}

package com.taxishare;
import com.taxishare.Activities.RegisterActivity;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EmailValidateTest {

    @Test
    public void emailValidator_CorrectEmailSimple_ReturnsTrue() throws Exception  {
        assertTrue(RegisterActivity.isValidEmail("name@email.com"));
    }

}